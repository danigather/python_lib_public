import random

class MyList(object):
    """List utilities.
    """
    @staticmethod
    def map(ls, function):
        """Apply function to each element of List.
        """
        new_ls = []
        for element in ls:
            result = function(element)
            new_ls.append(result)
        return new_ls

    @staticmethod
    def fold(ls, function, acc):
        """Apply function until list is empty.
        """
        for element in ls:
            acc = function(acc, element)
        return acc

    @staticmethod
    def foldl(x_xs, function, acc):
        """Apply function until list is empty.
        """
        if(x_xs == []):
            return acc
        else:
            x = x_xs[0]
            xs = x_xs[1:]
            acc = function(x, acc)
            return MyList.foldl(xs, function, acc)

    @staticmethod
    def foldr(xs_x, function, acc):
        """Apply function until list is empty.
        """
        if(xs_x == []):
            return acc
        else:
            x = xs_x[-1]
            xs = xs_x[:-1]
            acc = function(x, acc)
            return MyList.foldl(xs, function, acc)
        
    @staticmethod
    def apply(ls, function):
        """Apply function until list is empty.
        """
        len_ls = len(ls)
        pos = 0
        while pos < len_ls:
            element = ls[pos]
            restart = function(element)
            pos += 1
            if(restart == 0):
                pos = 0
        
    @staticmethod
    def clone(ls):
        """Clone one list.
        """
        return ls.copy()

    @staticmethod
    def lt_sort(ls_left, ls_right):
        """One method to compare with '<' two list if the list are sorted.
        Sort type like dictionary.
        """
        result = '='
        len_ls_left = len(ls_left)
        len_ls_right = len(ls_right)
        if(len_ls_left < len_ls_right):
            result = '<'
        elif(len_ls_left > len_ls_right):
            result = '>'
        min_len = min(len_ls_left, len_ls_right)
        for i in range(min_len-1, -1, -1):
            if(ls_left[i] < ls_right[i]):
                result = '<'
            elif(ls_left[i] > ls_right[i]):
                result = '>'            
        return result

    @staticmethod
    def select(ls, lsindex):
        """Select elements of a ls in positions given in lsindex.
        """
        new_ls = []
        for index in lsindex:
            new_ls.append(ls[index])
        return new_ls

    @staticmethod
    def join_inner(ls_left, ls_right):
        """Take elements of left ls not contained in right ls.
        """
        new_ls = []
        for element in ls_left:
            if(element in ls_right):
                new_ls.append(element)
        return new_ls

    @staticmethod
    def join_left_anti(ls_left, ls_right):
        """Take elements of left ls not contained in right ls.
        """
        new_ls = []
        for element in ls_left:
            if not(element in ls_right):
                new_ls.append(element)
        return new_ls

    @staticmethod
    def join_anti(ls_left, ls_right):
        """Take elements not in common.
        """
        new_ls = []
        for element in ls_left:
            if not(element in ls_right):
                new_ls.append(element)
        for element in ls_right:
            if not(element in ls_left):
                new_ls.append(element)
        return new_ls

    @staticmethod
    def transpose(lss):
        """Transpose a ls of ls.
        """
        new_lss = []
        for j in range(len(lss[0])):
            new_row = []
            for i in range(len(lss)):
                new_row.append(lss[i][j])
            new_lss.append(new_row)
        return new_lss

    @staticmethod
    def disjoint(ls_left, ls_right):
        """Return True if two ls don't have any element in common.
        """
        if(ls_left == []):
            return True
        else:
            boolean = not (ls_left[0] in ls_right)
            return boolean and MyList.disjoint(ls_left[1:], ls_right)

    @staticmethod
    def list_to_str(ls, sep=''):
        """Convert a list to a string.
        """
        cadena = ''
        for element in ls:
            cadena = cadena + str(element) + sep
        len_sep = len(sep)
        if(len_sep > 0):
            cadena = cadena[0:-len_sep]
        return cadena

    @staticmethod
    def print2(ls, sep='\n'):
        """Alternative print for list.
        """
        cadena = MyList.list_to_str(ls, sep=sep)
        print(cadena)

    @staticmethod
    def get_position_bigger(ls, n=1):
        """Return the position of n biggest elements of ls of numbers > 0.
        """
        len_ls = len(ls)
        get = min(len_ls, n)
        lstuple = []
        for i in range(len_ls):
            lstuple.append((ls[i],i))
        lstuple.sort(reverse=True)
        new_ls = []
        for i in range(get):
            new_ls.append(lstuple[i][1])
        return new_ls

    @staticmethod
    def crossover(lss):
        """Generate one list by random crossover of one matrix lss.
        """
        new_ls = []
        max_int = len(lss)-1
        for i in range(len(lss[0])):
            pos = random.randint(0, max_int)
            new_ls.append(lss[pos][i])
        return new_ls
        
    @staticmethod
    def generateMatrix(n, m):
        """Generate a Matrix of dimensions n x m.
        """
        lss = []
        for i in range(n):
            ls = []
            for j in range(m):
                ls.append([])
            lss.append(ls)
        return lss

    @staticmethod
    def append_not_repeated(ls, otro):
        """Append in ls objets of otro if not in ls.
        """
        if isinstance(otro, list):
            for element in otro:
                if(not(element in ls)):
                    ls.append(element)

        else:
            if(not(otro in ls)):
                ls.append(otro)

    @staticmethod
    def generate_random_subset(ls, len_subset):
        """Generate random subset of list with specified lenght.
        """
        new_ls = ls.copy()
        random.shuffle(new_ls)
        return new_ls[:len_subset]

    @staticmethod
    def get_first_compat(ls_1, ls_2, function_compat):
        """Get position of first element of ls_2 which is compatible with ls_1.
        """
        position = -1
        for i in range(len(ls_2)-1, -1, -1):
            boolean = True
            for element in ls_1:
                a = function_compat(element, ls_2[i])
                boolean = boolean and function_compat(element, ls_2[i])
            if boolean:
                position = i
        return position

    @staticmethod
    def is_compat(ls, function_compat):
        """Analize if all items of one list are compatible each other.
        """
        boolean = True
        len_ls = len(ls)
        for i in range(len_ls):
            for j in range(i+1, len_ls):
                ls_i = ls[i]
                ls_j = ls[j]
                boolean = boolean and function_compat(ls_i, ls_j)
        return boolean        

