from MyList import *

class Node(object):
    """Static tree class
        Args:
            -key: List of static position of Node in Tree
            -value: Value of Node
            -children: List of Nodes children of self
    """
    
    def __init__(self, key, value, children):
        """Initialize Node.
        """
        self.key = key
        self.value = value
        self.children = children

    def get_value(self):
        """Get self value.
        """
        return self.value

    def to_list(self):
        """Get node and all children in ls format.
        """
        def to_list_aux(node, ls_node):
            ls_node.append(node)
            for child in node.children:
                to_list_aux(child, ls_node)
        node = self
        ls_node = []
        to_list_aux(node, ls_node)
        return ls_node

    def get_ls_structure(self):
        """Get in ls format the structure of self.
        """
        def get_ls_structure_aux(node, ls_keys):
            ls_keys.append(node.key)
            for child in node.children:
                get_ls_structure_aux(child, ls_keys)
        node = self
        ls_keys = []
        get_ls_structure_aux(node, ls_keys)
        return ls_keys

    def get_element(self, key):
        """Get one node element from key starting in self.
        """
        start = len(self.key)
        key_copy = key.copy()[start:]
        node = self
        while(key_copy!=[]):
            position = key_copy.pop(0)
            node = node.children[position]
        return node

    def get_branch(self, key):
        """Get node branch from self until key position node.
        """
        start = len(self.key)
        key_copy = key.copy()[start:]
        node = self
        ls_node = [node]
        while(key_copy!=[]):
            position = key_copy.pop(0)
            node = node.children[position]
            ls_node.append(node)
        return ls_node

    def get_leaves(self):
        """Get all leaves of static tree from self.
        """
        ls_node = self.to_list()
        ls_leave = []
        for node in ls_node:
            if node.is_leave():
                ls_leave.append(node)
        return ls_leave

    def get_bigger(self):
        """Get biggest leave from self.
        """
        ls_leaves = self.get_leaves()
        ls_leaves_value = MyList.map(ls_leaves, Node.get_value)
        pos_winner = MyList.get_position_bigger(ls_leaves_value)[0]
        node_winner = ls_leaves[pos_winner]
        return node_winner        

    def __len__(self):
        """Get number of nodes from self.
        """
        ls_tree = self.to_list()
        nodes = len(ls_tree)
        return nodes
        
    def __eq__(self, otro):
        """Return self==otro.
        """
        boolean = False
        if isinstance(otro, Node):
            boolean = self.key == otro.key
            boolean = boolean and self.value == otro.value
        return boolean
    
    def get_children(self):
        """Return nodes children of self.
        """
        return self.children

    def __str__(self):
        """Return str(self).
        """
        ls_key = self.get_ls_structure()
        cadena = str(self.value) + '\n'
        key_principal = self.key
        start = len(key_principal)
        for key in ls_key[1:]:
            node = self.get_element(key)
            ls_len_children = self.get_ls_len_children(key)
            end = len(key)-1
            for i in range(start, end):
                if(key[i] == (ls_len_children[i]-1)):
                    cadena = cadena + '   '
                else:
                    cadena = cadena + '|  '
            cadena = cadena + '|---> '
            str_value = str(node.value)
            cadena = cadena + str_value + '\n'         
        return cadena

    def map(self, function=len):
        """Apply function to each element of the Tree.
        """
        new_value = function(self.value)
        new_ls_children = []
        for child in self.children:
            new_child = child.map(function)
            new_ls_children.append(new_child)
        node = Node(self.key, new_value, new_ls_children)
        return node

    def map_acum(self, function=sum, acc=0):
        """Apply function to each node of the tree with acc for each branch.
        """
        acc = function([self.value, acc])
        new_ls_children = []
        for child in self.children:
            new_child = child.map_acum(function, acc)
            new_ls_children.append(new_child)
        node = Node(self.key, acc, new_ls_children)
        return node

    def is_leave(self):
        """Return True if self has no children.
        """
        boolean = False
        if(self.children == []):
            boolean = True
        return boolean
        
    def is_root(self):
        """Return True if self has no parents.
        """
        boolean = False
        if(self.key == []):
            boolean = True
        return boolean

    def get_ls_len_children(self, key):
        """Get len of all generations of children of root until key Node.
        """
        ls_len_children = self.key.copy()
        start = len(ls_len_children)
        node = self
        key_copy = key.copy()
        key_copy = key_copy[start:]
        while(key_copy!=[]):
            position = key_copy.pop(0)
            ls_len_children.append(len(node.children))
            node = node.children[position]
        return ls_len_children
    
    def add_child(self, value):
        """Append child to node.
        """
        pos_child = len(self.children)
        new_key = self.key.copy()
        new_key.append(pos_child)
        node = Node(new_key, value, [])
        self.children.append(node)

    def add_children(self, ls_value):
        """Append multiple children to node.
        """
        for value in ls_value:
            self.add_child(value)

    def append_in_leaves_compatible_deprecated(self, ls_values, function_compatible):
        """This function is deprecated, please use append_in_leaves_compatible.
        """
        new_ls_values = function_compatible(self.value, ls_values)
        if self.is_leave():
            self.add_children(new_ls_values)
        else:
            for child in self.children:
                child.append_in_leaves_compatible_deprecated(new_ls_values, function_compatible)

    def append_in_leaves_compatible(self, ls_values, function_compatible, dict_compatible):
        """Append children in leaves compatible with all branch nodes.
        """
        new_ls_values = function_compatible(self.value, ls_values, dict_compatible)
        if self.is_leave():
            self.add_children(new_ls_values)
        else:
            for child in self.children:
                child.append_in_leaves_compatible(new_ls_values, function_compatible, dict_compatible)
