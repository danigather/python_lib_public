from MyList import *

class Tree(object):
    """Dynamic tree class
        Args:
            -lss: List of levels
    """
    
    def __init__(self, lss):
        """Initialize Tree.
        """
        self.tree = lss
        self.len = len(lss)
        self.ls_len = []
        self.update_ls_len()
        self.branch = []
        self.branch_value = []
        self.update_branch_left()

    def __str__(self, sep=', '):
        """Return str(self).
        """
        cadena = ''
        for i in range(self.len):
            j = self.branch[i]
            cadena = cadena + str(self.tree[i][j]) + sep
        cadena = cadena[:-len(sep)]
        return cadena

    def update_branch_value(self):
        """Update branch value from branch position.
        """
        self.branch_value = []
        for i in range(len(self.branch)):
            j = self.branch[i]
            if(j!=-1):
                value = self.tree[i][j]
                self.branch_value.append(value)

    def __len__(self):
        """Return self.len as Tree deepness.
        """
        return self.len

    def update_ls_len(self):
        """Update len value for each level of tree.
        """
        ls_len = []
        for ls in self.tree:
            ls_len.append(len(ls))
        self.ls_len = ls_len

    def update_branch_left(self):
        """Set branch to the left.
        """
        self.branch = self.len * [0]
        self.update_branch_value()

    def set_branch(self, ls):
        """Set branch in a specific position given.
        """
        self.branch = ls
        self.update_branch_value()

    def get_branch(self):
        """Get branch position.
        """
        return self.branch

    def get_branch_value(self):
        """Get branch value.
        """
        return self.branch_value

    def set_branch_right(self, n=1):
        """Move branch right to reach 'n' next Tree element.
        """
        quotient = n
        pos = self.len
        while(quotient!=0 and pos > 0):
            pos = pos -1
            value = self.branch[pos] + quotient
            rest = value % self.ls_len[pos]        
            quotient = value/self.ls_len[pos]
            self.branch[pos] = rest
        self.update_branch_value()

    def set_branch_right_empty(self):
        """Move branch right to reach next Tree element.\
            Possible to get incompatible level i with j<i.
            Possible to have new compatible level k>i.
        """
        boolean = True
        pos = self.len
        while(boolean and pos >= 0):
            pos = pos - 1
            value = self.branch[pos]
            if(value!=-1):
                boolean = False
                new_value = value + 1
                if(new_value == (self.ls_len[pos])):
                    new_value = -1
                    boolean = True
                self.branch[pos] = new_value
        self.update_branch_value()

    def complete_branch(self, function_compat):
        """Complete most left possible compatible branch.
        """
        pos = self.len
        value = -1
        while(pos>0 and value==-1):
            pos = pos - 1
            value = self.branch[pos]
        for i in range(pos+1, self.len):
            value = MyList.get_first_compat(self.branch_value, self.tree[i], function_compat)
            self.branch[i] = value
            self.update_branch_value()

    def set_branch_right_compat(self, function_compat):
        """Move branch right to reach next compatible Tree element.\
            Possible to get incompatible level i with j<i.
            Possible to have new compatible level k>i.
        """
        boolean = False
        while(not(boolean)):
            self.set_branch_right_empty()
            boolean = MyList.is_compat(self.branch_value, function_compat)
        self.complete_branch(function_compat)
        
    def in_tree(self):
        """Return if branch position get out of tree.
        """
        return self.branch[0]!=-1
        

