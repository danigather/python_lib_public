import inspect

class Test(object):
    """Test Class
        Args:
            -toTest: Class to be tested
    """

    def __init__(self, toTest):
        """Initialize Test objetct.
        """
        self.toTest = toTest
        self.list_method_inherited = ['__init__', '__module__', '__dict__', '__weakref__', '__doc__', '__hash__']
        self.list_method_noTest = []
        self.list_method = self.get_list_method()
        self.list_method_tested = []
        self.boolean_test = True
        self.total_test = 0

        self.list_ko_expected = []
        self.list_ko_result = []
        self.list_ko_method = []
        self.list_ko_testCod = []

    @staticmethod
    def compare(expected, result):
        """Compare expected and result elements with __eq__ methods if list of elements.
        """
        def compare_aux(expected, result):
            if(isinstance(expected, list) and isinstance(result, list)):
                if(expected == [] and result == []):
                    return True
                if((expected != [] and result == [])or(expected == [] and result != [])):
                    return False
                else:
                    booleano = (expected.pop()==result.pop())
                    return booleano and compare_aux(expected, result)
            else:
                booleano = (expected == result)
                return booleano
        expected_copy = expected
        result_copy = result        
        if(isinstance(expected, list) and isinstance(result, list)):
            expected_copy = expected.copy()
            result_copy = result.copy()
        return compare_aux(expected_copy, result_copy)

    def get_list_ko_expected(self):
        """Get list of expected tested ko's.
        """
        return self.list_ko_expected

    def get_list_ko_result(self):
        """Get list of result tested ko's.
        """
        return self.list_ko_result

    def get_list_method(self):
        dict_method = self.toTest.__dict__
        list_method = []
        list_method_detail = []
        for key, value in dict_method.items():
            if(not key in self.list_method_inherited
                and not key in self.list_method_noTest):
                list_method.append(key)
                list_method_detail.append(value)
        return list_method

    def _update_list_new_method(self):
        """Return list of boolean showing for each position if ko_method is new.
        """
        list_ko_newMethod = []
        for i in range(len(self.list_ko_method)):
            if(i==0):
                list_ko_newMethod.append(True)
            elif(self.list_ko_method[i]==self.list_ko_method[i-1]):
                list_ko_newMethod.append(False)
            else:
                list_ko_newMethod.append(True)
        return list_ko_newMethod

    def run_test_set_get(self, set_method_name='', get_method_name='', test_cod='', set_inputs='', get_inputs='', expected='', function=None):
        """Take one test,
        Apply the get method with the inputs to the class and compare the return of the
        class with the expected."""
        set_method = getattr(self.toTest, set_method_name)
        set_result = set_method(*set_inputs)
        get_method = getattr(self.toTest, get_method_name)
        get_result = get_method(*get_inputs)
        if(function == None):
            result = get_result
        else:
            result = function(get_result)
        boolean = Test.compare(expected,result)
        self.boolean_test = (self.boolean_test and boolean)
        self.total_test = self.total_test + 1
        if(not set_method_name in self.list_method_tested):
            self.list_method_tested.append(set_method_name)
        if(not get_method_name in self.list_method_tested):
            self.list_method_tested.append(get_method_name)
        if(not boolean):
            self.list_ko_expected.append(expected)
            self.list_ko_result.append(result)
            method_name = "SET: " + set_method_name + " |GET: " + get_method_name
            self.list_ko_method.append(method_name)
            self.list_ko_testCod.append(test_cod)

    def run_test_get(self, method_name='', test_cod='', inputs='', expected='', function=None):
        """Take one test,
        Apply to class get method with inputs,
        compare the return with the expected.
        """
        method = getattr(self.toTest, method_name)
        method_result = method(*inputs)
        if(function == None):
            result = method_result
        else:
            result = function(method_result)
        boolean = Test.compare(expected,result)
        self.boolean_test = (self.boolean_test and boolean)
        self.total_test = self.total_test + 1
        if(not method_name in self.list_method_tested):
            self.list_method_tested.append(method_name)
        if(not boolean):
            self.list_ko_expected.append(expected)
            self.list_ko_result.append(result)
            self.list_ko_method.append(method_name)
            self.list_ko_testCod.append(test_cod)

    def run_test_set(self, method_name='', test_cod='', inputs='', expected='', function=None):
        """Take one test,
        Apply to class get method with inputs,
        compare the class result with the expected.
        """
        method = getattr(self.toTest, method_name)
        method(*inputs)
        method_result=inputs[0]
        if(function == None):
            result = method_result
        else:
            result = function(method_result)
        boolean = Test.compare(expected,result)
        self.boolean_test = (self.boolean_test and boolean)
        self.total_test = self.total_test + 1
        if(not method_name in self.list_method_tested):
            self.list_method_tested.append(method_name)
        if(not boolean):
            self.list_ko_expected.append(expected)
            self.list_ko_result.append(result)
            self.list_ko_method.append(method_name)
            self.list_ko_testCod.append(test_cod)

    def run_test(self, method_name='', test_cod='', inputs='', expected='', function=None):
        """Take one test,
        Apply to class get method with inputs,
        compare (return or result) with the expected.
        If function method of class don´t have return because of a method like set or
        update, it compares the member of the class with the function applicated.
        """
        method = getattr(self.toTest, method_name)
        method_str = inspect.getsource(method)
        has_return = "return" in method_str
        method_result = method(*inputs)
        if(not(has_return)):
            method_result=inputs[0]

        if(function == None):
            result = method_result
        else:
            result = function(method_result)

        boolean = Test.compare(expected,result)
        self.boolean_test = (self.boolean_test and boolean)
        self.total_test = self.total_test + 1
        if(not method_name in self.list_method_tested):
            self.list_method_tested.append(method_name)
        if(not boolean):
            self.list_ko_expected.append(expected)
            self.list_ko_result.append(result)
            self.list_ko_method.append(method_name)
            self.list_ko_testCod.append(test_cod)
            
    def get_status(self):
        """Get True if all methods are OK.
        """
        return self.boolean_test

    def get_info(self):
        """Return log information of all test.
        """
        if(self.boolean_test):
            self.total_test
            cadena = '+------------------------------\n'
            cadena = cadena + '| OK :) -> ' + str(self.toTest) + '\n'
            cadena = cadena + '|>>> All test run succesfully\n'
            cadena = cadena + '|>>> Total test run: ' + str(self.total_test) + '\n'
            cadena = cadena + self.coverage()
            cadena = cadena + '+------------------------------\n'
        else:
            list_ko_newMethod = self._update_list_new_method()
            cadena = '+------------------------------\n'
            cadena = cadena + '| KO :( -> ' + str(self.toTest) + ' NO MATCH' + '\n'
            cadena = cadena + '+------------------------------\n'
            for i in range(len(self.list_ko_method)):
                expected = str(self.list_ko_expected[i])
                result = str(self.list_ko_result[i])
                method = str(self.list_ko_method[i])
                test_cod = str(self.list_ko_testCod[i])
                new_method = list_ko_newMethod[i]
                if(new_method):
                    cadena = cadena + '|>>> method: ' + method + '\n'           
                cadena = cadena + '|  -> Test: ' + test_cod + '\n'
                cadena = cadena + '|     Expected: ' + expected + '\n'
                cadena = cadena + '|     Result: ' + result + '\n'
            cadena = cadena + '+------------------------------\n'
        return cadena

    def __str__(self):
        """Print log.
        """
        cadena = self.get_info()
        return cadena

    def append_method_noTest(self, method):
        """Append method if no test are going to be set.
        """
        self.list_method_noTest.append(method)
        
    def get_method_noTested(self):
        """Get all methods haven´t been tested.
        """
        list_method = self.list_method
        list_method_tested = self.list_method_tested
        list_method_noTested = []
        for method in list_method:
            if(not method in list_method_tested):
                if(not method in self.list_method_noTest):
                    list_method_noTested.append(method)
        return list_method_noTested

    def is_total(self):
        """Return True if all methods of class have been tested.
        """
        boolean = False
        list_method_noTested = self.get_method_noTested()
        if(list_method_noTested == []):
            boolean = True
        return boolean

    def coverage(self):
        """Return string log for coverage.
        """
        if(self.is_total()):
            cadena = '|>>> All methods are tested\n'
        else:
            list_method_noTested = self.get_method_noTested()
            cadena = '|>>> The following methods are not tested:\n'
            for method in list_method_noTested:
                cadena = cadena + '|  -> ' + method + '\n'
        return cadena

