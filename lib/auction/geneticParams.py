import time
from MyList import *

class GeneticParams(object):
    def __init__(self):
        self.ls_score = []
        self.ls_solution = []

        self.selection_elements = 25
        self.crossover_elements = 50
        self.crossover_parents = 2
        self.mutation_elements = 25
        self.mutation_index = 0.2

        self.cicles = 100
        self.profit_cicle = []
        self.profit = 0
        
    def get_params_selection(self):
        return self.selection_elements
        
    def get_params_crossover(self):
        return (self.crossover_elements, self.crossover_parents)
        
    def get_params_mutation(self):
        return (self.mutation_elements, self.mutation_index)

    def set_params_selection(self, selection_elements=25):
        self.selection_elements = selection_elements
        
    def set_params_crossover(self, crossover_elements=50, crossover_parents=2):
        self.crossover_elements = crossover_elements
        self.crossover_parents = crossover_parents
        
    def set_params_mutation(self, mutation_elements=25, mutation_index=0.2):
        self.mutation_elements = mutation_elements
        self.mutation_index = mutation_index

    def initialize(self):
        """Initialize ls_distribution_items with n samples
        """
        pass

    def fitness(self):
        """Evalue all distributions
        """
        pass

    def selection_league(self):
        """Select n better distribution in a league format
        """
        pass

    def selection_cup(self):
        pass
    
    def selection(self):
        """Select n better distribution in a league format
        """
        self.selection_league()
        pass

    def crossover(self):
        """Cross distribution
        """
        pass

    def mutation(self):
        """Mutate distribution
        """
        pass

    def cicle(self):
        self.selection()
        self.crossover()
        self.mutation()
        self.fitness()
        profit_cicle = self.get_profit()
        self.profit_cicle.append(profit_cicle)
        self.profit = profit_cicle

    def iterate(self):
        self.initialize()
        self.fitness()
        for i in range(self.cicles):
            self.cicle()

    def solve(self, profit, max_time):
        start = time.time()
        total_time = 0
        self.initialize()
        self.fitness()
        while(self.profit < profit and total_time < max_time):
            self.cicle()
            stop = time.time()
            total_time = stop - start
        return total_time

    def get_profit(self):
        index = MyList.get_position_bigger(self.ls_score)
        profit = self.ls_score[index[0]]
        return profit

    def get_solution(self):
        index = MyList.get_position_bigger(self.ls_score)
        ls_winner_bid = self.ls_solution[index[0]]
        return ls_solution

    def get_total_cicles(self):
        return len(self.profit_cicle)
    
    def get_profit_cicle(self):
        return self.profit_cicle
    
