import random
from MyList import *
from auction.bid import *
from auction.auction import *
from auction.lsBid import *
from auction.geneticParams import *

class GeneticBid(Auction, GeneticParams):
    def __init__(self, ls_bid):
        Auction.__init__(self, ls_bid)
        GeneticParams.__init__(self)
        
        self.total_bids = len(self.ls_bid)
        self.total_bidders = len(self.ls_bidder)
        self.total_items = len(self.ls_item)
        
    def set_distribution_bids(self, ls_solution):
        self.ls_solution = ls_solution.copy()

    def get_distribution_bids(self):
        return self.ls_solution

    def get_ls_score(self):
        return self.ls_score

    def append_distribution_bids(self, distribution_bids):
        if(not(LsBid.in_ls(distribution_bids, self.ls_solution))):
            self.ls_solution.append(distribution_bids)
        else:
            distribution_bids = LsBid.get_random_compatible(self.ls_bid, self.dict_compat)
            self.append_distribution_bids(distribution_bids)

    def update_ls_score(self):
        self.ls_score = []
        for distribution_bids in self.ls_solution:
            score = 0
            for bid in distribution_bids:
                new_score = bid.get_score()
                score = score + new_score
            self.ls_score.append(score)
                        
    def initialize(self):
        """Initialize ls_distribution_items with n samples
        """
        self.ls_solution = []
        elements = self.selection_elements + self.crossover_elements + self.mutation_elements
        for i in range(elements):
            distribution_bids = LsBid.get_random_compatible(self.ls_bid, self.dict_compat)
            self.append_distribution_bids(distribution_bids)

    def fitness(self):
        """Evalue all distributions
        """
        self.update_ls_score()
        pass

    def selection_league(self, n):
        """Select n better distribution in a league format
        """
        index = MyList.get_position_bigger(self.ls_score, n)
        new_ls_solution = MyList.select(self.ls_solution, index)
        self.ls_solution = new_ls_solution

    def selection(self):
        """Select n better distribution in a league format
        """
        self.selection_league(self.selection_elements)

    def crossover(self):
        """Cross distribution
        """
        max_pos_distribution_bids = len(self.ls_solution)-1
        for i in range(self.crossover_elements):
            new_dist_bids = []
            for j in range(self.crossover_parents):
                pos = random.randint(0, max_pos_distribution_bids)
                new_dist_bids = new_dist_bids + self.ls_solution[pos].copy()
                new_dist_bids = LsBid.get_random_compatible(new_dist_bids, self.dict_compat)
            new_dist_bids = LsBid.get_complete_random_compatible(new_dist_bids, self.ls_bid, self.dict_compat)            
            self.append_distribution_bids(new_dist_bids)

    def mutation(self):
        """Mutate distribution
        """
        max_item = self.get_total_items() - 1
        max_pos_distribution_bids = len(self.ls_solution)-1
        for i in range(self.mutation_elements):
            pos = random.randint(0, max_pos_distribution_bids)
            new_distribution_bid = self.ls_solution[pos].copy()
            for j in range(len(new_distribution_bid) -1, -1, -1):
                random_mute = random.random()
                if(random_mute < self.mutation_index):
                    new_distribution_bid.pop(j)
            new_distribution_bid = LsBid.get_complete_random_compatible\
                                   (new_distribution_bid, self.ls_bid, self.dict_compat)
            self.append_distribution_bids(new_distribution_bid)        
        
