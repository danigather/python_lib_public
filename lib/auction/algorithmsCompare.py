from auction.auction import *
from auction.sandholm import *
from auction.sandholmDynamic import *
from auction.geneticBid import *
from auction.geneticItem import *

def gen_name(bidders, items, bids):
    str_bidders = '_B' + str(bidders)
    str_items = '_I' + str(items)
    str_bids = '_ID' + str(bids)
    info_auction = str_bidders + str_items + str_bids
    return info_auction

def easy_auction(bidders, items, bids, max_items, score):
    auction = Auction.initialize(bidders-1, items)
    auction.append_random_bids(bids, max_items, score)
    auction.initialize(bidders, items)
    auction.append_winner_bids(max_items, score)
    auction.pre()
    return auction

def new_auction(file_name, bidders, items, bids, max_items, score):
    file_path = 'result/auction' + info_auction + '.txt'
    auction = easy_auction(bidders, items, bids, max_items, score)
    auction.save(file_path)

def load_auction(file_name):
    file_path = 'result/auction' + file_name + '.txt'
    ls_bids = Auction.load(file_path)
    return ls_bids

def save(info_auction, ls_info):
    cadena ='ALGORITHM|PROFIT|TIME|ITERATIONS\n'
    for info in ls_info:
        line = str(info[0]) + '|' + str(info[1]) + '|' + str(info[2]) + '|' + str(info[3])
        cadena = cadena + line + '\n'
    fileName = 'result/log/auction' + info_auction + '_log.txt'
    file = open(fileName, 'w', encoding='utf-8')
    file.write(cadena)
    file.close()

def compare(ls_bids):
    sandholm = Sandholm(ls_bids)
    geneticBid = GeneticBid(ls_bids)
    geneticItem = GeneticItem(ls_bids)
    
    time_sandholm = sandholm.solve()
    total_profit = sandholm.get_profit()
    algorithm1 = ['Sandholm', total_profit, time_sandholm, 'N/A']
    max_time = 3 * time_sandholm

    time_geneticBid = geneticBid.solve(total_profit, max_time)
    profit_geneticBid = geneticBid.get_profit()
    iter_geneticBid = geneticBid.get_total_cicles()
    algorithm2 = ['GeneticBid', profit_geneticBid, time_geneticBid, iter_geneticBid]

    time_geneticItem = geneticItem.solve(total_profit, max_time)
    profit_geneticItem = geneticItem.get_profit()
    iter_geneticItem = geneticItem.get_total_cicles()
    algorithm3 = ['GeneticItem', profit_geneticItem, time_geneticItem, iter_geneticItem]

    ls_algorithm = [algorithm1, algorithm2, algorithm3]
    return ls_algorithm

def comparation(bidders, items, bids):
    cadena = gen_name(bidders, items, bids)
    new_auction(cadena, bidders, items, bids, 5, 1)
    ls_bids = load_auction(cadena)
    ls_info = compare(ls_bids)
    save(cadena, ls_info)

def multiple_comparation(bidders_number, items_number, bids_number):
    for bidders in bidders_number:
        for items in items_number:
            for bids in bids_number:
                comparation(bidders, items, bids)

def run_geneticItem(bidders, items, bids):
    auction = easy_auction(bidders, items, bids, 5, 1)
    ls_bid = auction.ls_bid
    ls_bid_number = len(auction.ls_bid)
    genetic = GeneticItem(ls_bid)
    cadena = 'START: ' 
    cadena = cadena + 'Bidders: ' + str(bidders)
    cadena = cadena +', Items: ' + str(items)
    cadena = cadena + ', Bids: ' + str(bids)
    print(cadena)
    #Para parar por profit
    profit = items * 1
    max_time = bids
    time_genetic = genetic.solve(profit, max_time)
    total_profit = genetic.get_profit()

    cicles = genetic.get_total_cicles()
    cadena = 'Cicles: ' + str(cicles) + ' -> '
    cadena = cadena + 'Bids: ' + str(len(genetic))
    cadena = cadena + ', Time: ' + str(time_genetic)
    cadena = cadena + ', Profit: ' + str(total_profit)
    print(cadena)
    return genetic

def run_geneticBid(bidders, items, bids):
    auction = easy_auction(bidders, items, bids, 5, 1)
    ls_bid = auction.ls_bid
    ls_bid_number = len(auction.ls_bid)
    genetic = GeneticBid(ls_bid)
    cadena = 'START: ' 
    cadena = cadena + 'Bidders: ' + str(bidders)
    cadena = cadena +', Items: ' + str(items)
    cadena = cadena + ', Bids: ' + str(bids)
    print(cadena)
    #Para parar por profit
    profit = items * 1
    max_time = bids
    time_genetic = genetic.solve(profit, max_time)
    total_profit = genetic.get_profit()

    cicles = genetic.get_total_cicles()
    cadena = 'Cicles: ' + str(cicles) + ' -> '
    cadena = cadena + 'Bids: ' + str(len(genetic))
    cadena = cadena + ', Time: ' + str(time_genetic)
    cadena = cadena + ', Profit: ' + str(total_profit)
    print(cadena)
    return genetic
    
def run_sandholm(auction):
    ls_bid = auction.ls_bid
    sandholm = Sandholm(ls_bid)

    time_sandholm = sandholm.solve()
    total_profit = sandholm.get_profit()
    total_nodes = len(sandholm.search1)
    
    info = [total_nodes, time_sandholm]
    return info

def run_ls_sandholm(ls_bidders, ls_items, ls_bids):
    total_info_sandholm = ''
    for bidders in ls_bidders:
        for items in ls_items:
            for bids_loser in ls_bids:
                auction = easy_auction(bidders, items, bids_loser, 5, 1)
                key_str = str(bidders) + '-' + str(items) + '-' + str(bids_loser)
                bids = len(auction)
                print(key_str)
                ls_info_sandholm = run_sandholm(auction)
                ls_info = [key_str, bids] + ls_info_sandholm
                info_sandholm = MyList.list_to_str(ls_info, ' & ') + ' \\\\\n' + '\hline' + '\n'
                total_info_sandholm = total_info_sandholm + info_sandholm
    return total_info_sandholm

def run_sandholmDynamic(auction):
    ls_bid = auction.ls_bid
    sandholm = SandholmDynamic(ls_bid)

    time_sandholm = sandholm.solve()
    total_profit = sandholm.get_profit()
    total_branch = sandholm.get_total_branch()

    info = [total_branch, time_sandholm]
    return info

def run_ls_sandholmDynamic(ls_bidders, ls_items, ls_bids):
    total_info_sandholm = ''
    for bidders in ls_bidders:
        for items in ls_items:
            for bids_loser in ls_bids:
                auction = easy_auction(bidders, items, bids_loser, 5, 1)
                key_str = str(bidders) + '-' + str(items) + '-' + str(bids_loser)
                bids = len(auction)
                print(key_str)
                ls_info_sandholmDynamic = run_sandholmDynamic(auction)
                static = '\multicolumn{2}{c||}{-1}'
                ls_info = [key_str, bids, static] + ls_info_sandholmDynamic
                info_sandholm = MyList.list_to_str(ls_info, ' & ') + ' \\\\\n' + '\hline' + '\n'
                total_info_sandholm = total_info_sandholm + info_sandholm
    return total_info_sandholm

def run_compare_sandholm(ls_bidders, ls_items, ls_bids):
    total_info_sandholm = ''
    for bidders in ls_bidders:
        for items in ls_items:
            for bids_loser in ls_bids:
                auction = easy_auction(bidders, items, bids_loser, 5, 1)
                key_str = str(bidders) + '-' + str(items) + '-' + str(bids_loser)
                bids = len(auction)
                print(key_str)
                ls_info_sandholm = run_sandholm(auction)
                ls_info_sandholmDynamic = run_sandholmDynamic(auction)
                ls_info = [key_str, bids] + ls_info_sandholm + ls_info_sandholmDynamic
                info_sandholm = MyList.list_to_str(ls_info, ' & ') + ' \\\\\n' + '\hline' + '\n'
                total_info_sandholm = total_info_sandholm + info_sandholm
    return total_info_sandholm

def cicles_percentage(n, ls_cicle):
    per_20 = n * 0.2
    per_40 = n * 0.4
    per_60 = n * 0.6
    per_70 = n * 0.7
    per_75 = n * 0.75
    per_80 = n * 0.8
    per_85 = n * 0.85
    per_90 = n * 0.9
    per_95 = n * 0.95
    per_97 = n * 0.97
    per_99 = n * 0.99
    per_100 = n
    ls_per = [per_20, per_40, per_60, per_70, per_75, per_80, per_85, per_90, per_95, per_97, per_99, per_100]
    ls_info = len(ls_per)*[-1]
    cicles = len(ls_cicle)
    for i in range(cicles-1, -1, -1):
        for j in range(len(ls_per)):
            if(ls_cicle[i] >= ls_per[j]):
                ls_info[j] = i
    return ls_info

def compare_genetic(bidders, items, bids):
    cadena = str(bidders)+ '-' + str(items) + '-' + str(bids)
    
    genetic_item = run_geneticItem(bidders, items, bids)
    ls_cicle_item = genetic_item.get_profit_cicle()
    ls_info_item = cicles_percentage(items, ls_cicle_item)

    genetic_bid = run_geneticBid(bidders, items, bids)
    ls_cicle_bid = genetic_bid.get_profit_cicle()
    ls_info_bid = cicles_percentage(items, ls_cicle_bid)
    return (cadena, ls_info_item, ls_info_bid)

def compare_ls_genetic(ls_bidders, ls_items, ls_bids):
    total_info_item = ''
    total_info_bid = ''
    for bidders in ls_bidders:
        for items in ls_items:
            for bids in ls_bids:
                info = compare_genetic(bidders, items, bids)
                (cadena, ls_info_item, ls_info_bid) = info
                info_item = cadena + ' & ' + MyList.list_to_str(ls_info_item, ' & ') + ' \\\\\n' + '\hline' + '\n'
                info_bid = cadena + ' & ' + MyList.list_to_str(ls_info_bid, ' & ') + ' \\\\\n' + '\hline' + '\n'
                total_info_item = total_info_item + info_item
                total_info_bid = total_info_bid + info_bid
    return (total_info_item, total_info_bid)

if __name__ == '__main__':
    ls_bidders = [20]#[5, 10, 20]
    ls_items = [30]#[10, 20, 30, 50, 100]
    ls_bids = [200]#40, 50, 60, 80, 100, 200, 500, 1000]
    #info = run_compare_sandholm(ls_bidders, ls_items, ls_bids)
    #info = run_ls_sandholm(ls_bidders, ls_items, ls_bids)
    info = run_ls_sandholmDynamic(ls_bidders, ls_items, ls_bids)
    #info = compare_ls_genetic(ls_bidders, ls_items, ls_bids)
    #(info_item, info_bid) = info
