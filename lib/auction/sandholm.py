import time
import MyList
from node import *
from auction.bid import *
from auction.auction import *
from auction.lsBid import *

class Sandholm(Auction):
    def __init__(self, ls_bid_init):
        Auction.__init__(self, ls_bid_init, True)
        self.search1 = Node([], Bid.empty(), [])
        self.tree_profit = Node([], 0, [])
        self.ls_winner_bid = []
        self.profit = 0

    def __str__(self):
        cadena = str(self.search1)
        return cadena

    def update_search1_deprecated(self):
        lss_bid = LsBid.cluster(self.ls_bid, self.ls_item)
        for ls_bid in lss_bid:
            self.search1.append_in_leaves_compatible(ls_bid, Bid.get_compatible_deprecated)

    def update_search1(self):
        lss_bid = LsBid.cluster(self.ls_bid, self.ls_item)
        for ls_bid in lss_bid:
            self.search1.append_in_leaves_compatible(ls_bid, Bid.get_compatible, self.dict_compat)
            
    def get_search1(self):
        return self.search1

    def update_tree_profit(self):
        tree_score = self.search1.map(Bid.get_score)
        tree_profit = tree_score.map_acum(function=sum, acc=0)
        self.tree_profit = tree_profit
    
    def get_tree_profit(self):
        return self.tree_profit
    
    def update_winner_bids(self):
        winner_leave = self.tree_profit.get_bigger()
        self.profit = winner_leave.value
        winner_key = winner_leave.key
        winner_branch = self.search1.get_branch(winner_key)
        ls_winner_dummy_bid = MyList.map(winner_branch, Node.get_value)
        ls_winner_bid = []
        for bid in ls_winner_dummy_bid:
            if(bid.score!=0):
                ls_winner_bid.append(bid)
        self.ls_winner_bid = ls_winner_bid

    def solve(self, log=False):
        start = time.time()
        self.update_search1()
        if log:
            print('search1')
        self.update_tree_profit()
        if log:
            print('profit')
        self.update_winner_bids()
        if log:
            print('winner_bids')
        stop = time.time()
        total_time = stop - start
        return total_time
    
    def get_solution(self):
        return self.ls_winner_bid
        
    def get_profit(self):
        return self.profit
