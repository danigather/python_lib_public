import random
from MyList import *
from auction.bid import *

class LsBid(object):

    @staticmethod
    def get_random_compatible(ls_bid, dict_compat):
        winner = []
        unknown = ls_bid
        while(unknown != []):
            max_pos_bids = len(unknown) - 1
            pos = random.randint(0, max_pos_bids)
            winner.append(unknown[pos])
            unknown = unknown[pos].get_compatible(unknown, dict_compat)
        winner.sort()
        return winner

    @staticmethod
    def cluster(ls_bid, ls_item):
        lss_bid = []
        for item in ls_item:
            ls_bid_item = []
            for bid in ls_bid:
                if(bid.items[0]==item):
                    ls_bid_item.append(bid)
            lss_bid.append(ls_bid_item)
        return lss_bid    

    @staticmethod
    def eq(ls_bid1, ls_bid2):
        boolean = True
        disjoint = MyList.join_anti(ls_bid1, ls_bid2)
        if (disjoint != []):
            boolean = False
        return boolean

    @staticmethod
    def in_ls(ls_bid, lss_bid):
        """return True if ls_bid is in lss_bid
        """
        if(lss_bid == []):
            return False
        else:
            ls_bid2 = lss_bid[0]
            boolean = LsBid.eq(ls_bid,ls_bid2)
            return boolean or LsBid.in_ls(ls_bid, lss_bid[1:])

    @staticmethod
    def extract_ls_compatible(ls_bid, ls_bid_toExtract):
        """Method to know all bids in ls_bid_toExtract that are compatible
        with ls_bid
        """
        ls_bid_extract_compatible = []
        for bid in ls_bid_toExtract:
            if(bid.is_compatible_with(ls_bid)):
                ls_bid_extract_compatible.append(bid)
        return ls_bid_extract_compatible
    
    @staticmethod
    def get_complete_random_compatible(ls_bid, ls_bid_toAppend, dict_compat):
        """Method to complete ls_bid with compatible bids in ls_bid_toAppend
        """
        ls_bid_compatible = LsBid.extract_ls_compatible(ls_bid, ls_bid_toAppend)
        bids_toAppend = LsBid.get_random_compatible(ls_bid_compatible, dict_compat)
        complete_random_compatible = ls_bid + bids_toAppend
        return complete_random_compatible

    @staticmethod
    def get_first_compat(ls_bid, ls_new_bid, dict_compat):
        position = -1
        for i in range(len(ls_new_bid)-1, -1, -1):
            boolean = ls_new_bid[i].is_compat_ls(ls_bid, dict_compat)
            if boolean:
                position = i
        return position

    @staticmethod
    def get_profit(ls_bid):
        profit = 0
        for bid in ls_bid:
            profit = profit + bid.score
        return profit

    
