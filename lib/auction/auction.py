import MyList
from auction.bid import *

class Auction(object):
    def __init__(self, ls_bid_init, dummy=False):
        self.ls_bid_init = ls_bid_init
        self.ls_item = []
        self.ls_bidder = []
        self.update_items()
        self.update_bidders()
        self.ls_dummy_bid = []
        self.dummy = dummy
        self.update_dummy()
        self.ls_bid = []
        self.pre()
        self.dict_compat = {}
        self.update_dict_compat()

    def update_dummy(self):
        self.ls_dummy_bid = []
        if self.dummy:            
            long_str_item = len(str(len(self.ls_item)))
            bidder = 'B_'
            for i in range(len(self.ls_item)):
                item = self.ls_item[i]
                pk = 'DID_' + str(i).zfill(long_str_item)
                bid = Bid(pk, bidder, [item], 0)
                self.ls_dummy_bid.append(bid)
            
    def pre(self):
        bid_empty = Bid.empty()
        ls_bid = [bid_empty] + self.ls_bid_init + self.ls_dummy_bid
        ls_bid.sort()   
        compare = bid_empty
        new_ls_bid = []
        for bid in ls_bid:
            if(not(bid.is_worse(compare))):
                new_ls_bid.append(bid)
                compare = bid
        self.ls_bid = new_ls_bid

    def update_dict_compat(self):
        bid_empty = Bid.empty()
        ls_bid = [bid_empty] + self.ls_bid
        for bid1 in ls_bid:
            dict_bid = {}
            key1 = bid1.id
            for bid2 in ls_bid:
                key2 = bid2.id
                boolean = bid1.is_compatible_with(bid2)
                dict_bid.update({key2:boolean})
            self.dict_compat.update({key1:dict_bid})

    def get_dict_compat(self):
        return self.dict_compat
    
    def bid_compat(self, bid1, bid2):
        key1 = bid1.id
        key2 = bid2.id
        return self.dict_compat.get(key1).get(key2)
    
    def __str__(self):
        cadena = ''
        ls_bid = self.ls_bid
        for bid in ls_bid:
            cadena = cadena + str(bid) + '\n'
        return cadena

    def __eq__(self, otro):        
        join_anti = MyList.join_anti(self.ls_bid_init, otro.ls_bid_init)
        boolean = (join_anti == [])
        return boolean

    def __len__(self):
        number_bids = len(self.ls_bid)
        return number_bids

    def update_bidders(self):
        ls_bidder = []
        for bid in self.ls_bid_init:
            bid_bidder = bid.get_bidder()
            MyList.append_not_repeated(ls_bidder, bid_bidder)
        ls_bidder.sort()
        self.ls_bidder = ls_bidder
    
    def update_items(self):
        ls_item = []
        for bid in self.ls_bid_init:
            bid_items = bid.get_items()
            MyList.append_not_repeated(ls_item, bid_items)
        ls_item.sort()
        self.ls_item = ls_item

    def set_bidders(self, ls_bidder):
        self.ls_bidder = ls_bidder
    
    def set_items(self, ls_item):
        self.ls_item = ls_item

    def get_bidders(self):
        return self.ls_bidder
    
    def get_items(self):
        return self.ls_item
    
    @staticmethod
    def initialize(bidders, items):
        long_str_bidders = len(str(bidders))
        ls_bidder = []
        for i in range(bidders):
            bidder = 'B' + str(i).zfill(long_str_bidders)
            ls_bidder.append(bidder)
        long_str_items = len(str(items))
        ls_item = []
        for i in range(items):
            item = 'I' + str(i).zfill(long_str_items)
            ls_item.append(item)
        new_auction = Auction([])
        new_auction.set_bidders(ls_bidder)
        new_auction.set_items(ls_item)
        return new_auction

    def append_random_bids(self, bids, max_items, max_score, min_score=0):
        long_str_bids = len(str(bids))
        for i in range(bids):
            pk = 'LID' + str(i).zfill(long_str_bids)
            bidder = random.choice(self.ls_bidder)
            len_items = random.randint(1, max_items)
            items = MyList.generate_random_subset(self.ls_item, len_items)
            dif_score_item = max_score - min_score
            dif_score = dif_score_item * random.random()
            score = len_items * (min_score + dif_score)        
            bid = Bid(pk, bidder, items, score)
            self.ls_bid_init.append(bid)

    def append_winner_bids(self, max_items, score):
        long_str_bids = len(str(len(self.ls_bid)))
        i = 0
        bidder = self.ls_bidder[-1]
        start = 0
        final = random.randint(1, max_items)
        len_items = len(self.ls_item)
        while(start < len_items):
            items = self.ls_item[start:final]
            score_items = len(items) * score
            pk = 'WID' + str(i).zfill(long_str_bids)
            i+=1
            bid = Bid(pk, bidder, items, score_items)
            self.ls_bid_init.append(bid)
            start = final
            random_items = random.randint(1, max_items)
            final = start + random_items

    @staticmethod
    def load(path):
        file = open(path, 'r', encoding='utf-8')
        text = file.read()
        file.close()
        ls_str_bids = text.split('\n')
        while(ls_str_bids[-1]==''):
            ls_str_bids = ls_str_bids[:-1]
        ls_bids = []
        for str_bid in ls_str_bids:
            ls_info_bid = str_bid.split('|')
            pk = ls_info_bid[0]
            bidder = ls_info_bid[1]
            ls_items = ls_info_bid[2][1:-1].split(',')
            score = float(ls_info_bid[3])
            bid = Bid(pk, bidder, ls_items, score)
            ls_bids.append(bid)
        return ls_bids

    def save(self, path):
        file = open(path, 'w', encoding='utf-8')
        for bid in self.ls_bid:
            file.write(str(bid)+'\n')
        file.close()
        
    def clone(self):
        new_ls_bid = self.ls_bid_init.copy()
        dummy = self.dummy
        new_auction = Auction(new_ls_bid, dummy)
        return new_auction

    def sort_ls_bid(self):
        self.ls_bid.sort()

    def set_bids(self, ls_bid):
        self.ls_bid = ls_bid

    def get_dummy_bids(self):
        return self.ls_dummy_bid
    
    def get_bids(self):
        return self.ls_bid

    def get_id(self):
        ls_id = []
        for bid in self.ls_bid:
            bid_id = bid.get_id()
            MyList.append_not_repeated(ls_id, bid_id)
        return ls_id

    def get_total_bidders(self):
        return len(self.ls_bidder)
    
    def get_total_items(self):
        return len(self.ls_item)
    
    def get_bids_from_bidder(self, bidder):
        ls_bids = []
        for bid in self.ls_bid:
            bid_bidder = bid.get_bidder()
            if(bid_bidder == bidder):
                ls_bids.append(bid)
        return ls_bids
