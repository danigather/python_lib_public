from MyList import *

class Bid:
    def __init__(self, pk, bidder, items, score):
        self.id = pk
        self.bidder = bidder
        self.items = items.copy()
        self.items.sort()
        self.score = score

    @staticmethod
    def empty():
        pk = 'DID_'
        bidder = 'B_'
        items = []
        score = 0
        empty_bid = Bid(pk, bidder, items, score)
        return empty_bid
    
    def __len__(self):
        return len(self.items)

    def __str__(self):
        items = ''
        for item in self.items:
            items = items + str(item) + ','
        items = items[:-1]
        cadena = str(self.id) + '|' + str(self.bidder) + \
        '|[' + str(items) + ']|' + str(self.score)
        return cadena

    def __eq__(self, otro):
        """Implement '==' method
        The result compares:
            1. Bidder
            2. Items
        """
        boolean = False
        if isinstance(otro, Bid):
            if(self.id == otro.id):
                if(self.bidder == otro.bidder):
                    if(len(self)==len(otro)):
                        if(self.items == otro.items):
                            if(self.score == otro.score):
                                boolean = True
        return boolean

    def __lt__(self, otro):
        """Implement '<' method in order to apply ordering bids
        The order is given by:
            1. Smaller items
            3. Biggest score
            4. Small bidder
        """
        sign = MyList.lt_sort(self.items, otro.items)
        if(sign == '<'):
            boolean = True
        elif(sign == '>'):
            boolean = False
        else:
            if(self.score > otro.score):
                boolean = True
            elif(self.score < otro.score):
                boolean = False
            else:
                if(self.bidder < otro.bidder):
                    boolean = True
                else:
                    boolean = False
        return boolean
    
    def get_id(self):
        return self.id
    
    def get_bidder(self):
        return self.bidder

    def get_items(self):
        return self.items

    def get_score(self):
        return self.score

    def clone(self):
        pk = self.get_id()
        bidder = self.get_bidder()
        items = self.get_items()
        score = self.get_score()
        newBid = Bid(pk, bidder, items, score)
        return newBid

    def disjoint(self, otro):
        join_anti = MyList.join_anti(self.items, otro.items)
        boolean = (join_anti == [])
        return boolean
   
    def is_contained(self, ls_items):
        """Function to know if al items of the Bid are in a given ls
        """
        boolean = True
        for item in self.items:
            boolean = boolean and (item in ls_items)
        return boolean
    
    def get_score_of_items(self, ls_items):
        """Function to know if al items of a ls are in the Bid
        """
        score = 0
        if(self.is_contained(ls_items)):
            score = self.score
            ls_items = MyList.join_left_anti(ls_items, self.items)
        return (score, ls_items)
    
    def is_compatible_with(self, otro):
        """Function to know compatibleibility of a bid with other bid or ls(Bid)
        """
        if(isinstance(otro, Bid)):
            return MyList.disjoint(self.items, otro.items)
        elif(isinstance(otro, list)):
            if(otro==[]):
                return True
            else:
                bid = otro[0]
                ls_ = otro[1:]
                return (self.is_compatible_with(bid) and self.is_compatible_with(ls_))

    def is_compat(self, otro, dict_compat):
        key1 = self.id
        key2 = otro.id
        boolean = dict_compat.get(key1).get(key2)
        return boolean

    def is_compat_ls(self, x_xs, dict_compat):
        if(x_xs == []):
            return True
        else:
            x = x_xs[0]
            xs = x_xs[1:]
            boolean = self.is_compat(x, dict_compat)
            return boolean and self.is_compat_ls(xs, dict_compat)
        
    def get_compatible_deprecated(self, otro):
        ls_compatible = []
        for bid_compare in otro:
            if(self.is_compatible_with(bid_compare)):
                ls_compatible.append(bid_compare)
        return ls_compatible

    def get_compatible(self, otro, dict_compat):
        ls_compatible = []
        for bid_compare in otro:
            if(self.is_compat(bid_compare, dict_compat)):
                ls_compatible.append(bid_compare)
        return ls_compatible
    
    def is_from_bidder(self, bidder):
        boolean = False
        if bidder == self.bidder:
            boolean = True
        return boolean

    def is_worse(self, otro):
        boolean = False
        if(self.items == otro.items):
            if(self.score <= otro.score):
                boolean = True
        return boolean

    def sum_score(self, otro):
        return self.score + otro.score
        

    
