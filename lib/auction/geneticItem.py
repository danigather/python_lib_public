import random
from MyList import *
from auction.bid import *
from auction.auction import *
from auction.geneticParams import *

class GeneticItem(Auction, GeneticParams):
    def __init__(self, ls_bid):
        Auction.__init__(self, ls_bid)
        GeneticParams.__init__(self)

        self.total_bidders = len(self.ls_bidder)
        self.total_items = len(self.ls_item)
        
        self.ls_distribution_items = []
        self.ls_distribution_bidders = []
        
    def set_distribution_items(self, ls_distribution_items):
        self.ls_distribution_items = ls_distribution_items

    def get_distribution_items(self):
        return self.ls_distribution_items

    def get_distribution_bidders(self):
        return self.ls_distribution_bidders

    def get_ls_score(self):
        return self.ls_score
    
    def get_random_distribution(self):
        max_pos_bidders = self.get_total_bidders() - 1
        distribution = []
        for item in self.ls_item:
            position = random.randint(0, max_pos_bidders)
            distribution.append(position)
        return distribution

    def append_distribution_items(self, distribution_items):
        if(not(distribution_items in self.ls_distribution_items)):
            self.ls_distribution_items.append(distribution_items)
        else:
            distribution_items = self.get_random_distribution()
            self.append_distribution_items(distribution_items)

    def update_distribution_bidders(self):
        self.ls_distribution_bidders = []

        for distribution_items in self.ls_distribution_items:
            new_distribution_bidders = MyList.generateMatrix(self.total_bidders, 0)
            for itemPos in range(self.total_items):
                bidderPos = distribution_items[itemPos]
                item = self.ls_item[itemPos]
                new_distribution_bidders[bidderPos].append(item)
            self.ls_distribution_bidders.append(new_distribution_bidders)

            
    def update_ls_score(self):
        len_to_append = len(self.ls_distribution_items) - len(self.ls_score)
        self.ls_score = self.ls_score + len_to_append * [0]
        self.ls_solution = self.ls_solution + len_to_append * [0]
        for i in range(len(self.ls_distribution_bidders)):
            distribution_bidders = self.ls_distribution_bidders[i]
            score = 0
            winner_bids = []
            for index_bidder in range(self.total_bidders):
                bidder = self.ls_bidder[index_bidder]
                ls_item = distribution_bidders[index_bidder]
                ls_bids = self.get_bids_from_bidder(bidder)
                random.shuffle(ls_bids)
                for bid in ls_bids:
                    if bid.is_contained(ls_item):
                        score = score + bid.score
                        winner_bids.append(bid)
                        ls_item = MyList.join_anti(bid.items, ls_item)
            if(score > self.ls_score[i]):
                self.ls_solution[i] = winner_bids
                self.ls_score[i] = score
                        
    def initialize(self):
        """Initialize ls_distribution_items with n samples
        """
        self.ls_distribution_items = []
        elements = self.selection_elements + self.crossover_elements + self.mutation_elements
        for i in range(elements):
            distribution_items = self.get_random_distribution()
            self.append_distribution_items(distribution_items)

    def fitness(self):
        """Evalue all distributions
        """
        self.update_distribution_bidders()
        self.update_ls_score()

    def selection_league(self):
        """Select n better distribution in a league format
        """
        index = MyList.get_position_bigger(self.ls_score, self.selection_elements)
        new_ls_distribution_items = MyList.select(self.ls_distribution_items, index)
        new_ls_distribution_bidders = MyList.select(self.ls_distribution_bidders, index)
        new_ls_solution = MyList.select(self.ls_solution, index)
        new_ls_score = MyList.select(self.ls_score, index)
        self.ls_distribution_items = new_ls_distribution_items
        self.ls_solution = new_ls_solution
        self.ls_score = new_ls_score
    
    def selection(self):
        """Select n better distribution in a league format
        """
        self.selection_league()

    def crossover(self):
        """Cross distribution
        """
        total_distribution_items = len(self.ls_distribution_items)-1
        for i in range(self.crossover_elements):
            l=[]
            for j in range(self.crossover_parents):
                pos = random.randint(0, total_distribution_items)
                l.append(self.ls_distribution_items[pos])
            new_distribution_item = MyList.crossover(l)
            self.append_distribution_items(new_distribution_item)

    def mutation(self):
        """Mutate distribution
        """
        max_item = self.get_total_items() - 1
        max_bidder = self.get_total_bidders() - 1
        total_distribution_items = len(self.ls_distribution_items)-1
        for i in range(self.mutation_elements):
            pos = random.randint(0, total_distribution_items)
            new_distribution_item = self.ls_distribution_items[pos].copy()
            for j in range(len(new_distribution_item)):
                random_mute = random.random()
                if(random_mute < self.mutation_index):
                    random_bidder = random.randint(0, max_bidder)
                    new_distribution_item[j] = random_bidder
            self.append_distribution_items(new_distribution_item)


