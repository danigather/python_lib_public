import time
import MyList
from tree import *
from auction.bid import *
from auction.auction import *
from auction.lsBid import *

class SandholmDynamic(Auction, Tree):
    def __init__(self, ls_bid_init):
        Auction.__init__(self, ls_bid_init, True)
        lss_bid = LsBid.cluster(self.ls_bid, self.ls_item)
        Tree.__init__(self, lss_bid)
        self.total_branch = 0
        self.branch_winner = []
        self.profit = 0

    def update_branch_left_compat(self):
        self.branch = []
        self.branch_value = []
        self.total_branch = 0
        for i in range(self.len):
            ls_level = self.tree[i]
            pos = LsBid.get_first_compat(self.branch_value, ls_level, self.dict_compat)
            self.branch.append(pos)
            self.update_branch_value()

    def explore_right(self):
        self.set_branch_right_compat(self.bid_compat)
        self.total_branch = self.total_branch + 1
        branch_profit = LsBid.get_profit(self.branch_value)
        if(branch_profit > self.profit):
            self.profit = branch_profit
            self.branch_winner = self.branch.copy()
        
    def __str__(self):
        cadena = str(self.profit)
        cadena = cadena + '->'
        cadena = cadena + str(self.branch_winner)
        return cadena

    def solve(self, log=False):
        start = time.time()
        self.update_branch_left_compat()
        while(self.in_tree()):
            self.explore_right()
        stop = time.time()
        total_time = stop - start
        return total_time
    
    def get_solution(self):
        self.set_branch(self.branch_winner.copy())
        return self.branch_value
        
    def get_profit(self):
        return self.profit

    def get_total_branch(self):
        return self.total_branch
