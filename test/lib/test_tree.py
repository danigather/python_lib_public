from test import *
from tree import *

if __name__ == '__main__':
    toTest = Tree
    test = Test(toTest)

    test.append_method_noTest('complete_branch')
    test.append_method_noTest('set_branch_right_compat')
    
    tree_lss = []
    tree_lss.append(['00', '01', '02'])
    tree_lss.append(['10', '11', '12', '13'])
    tree_lss.append(['20', '21'])
    tree_lss.append(['30'])
    tree_lss.append(['40', '41'])

    tree = Tree(tree_lss)

    tree_len = 5
    tree_ls_len = [3, 4, 2, 1, 2]

    branch_left_0 = [0, 0, 0, 0, 0]
    branch_left_1 = [0, 0, 0, 0, 1]
    branch_left_2 = [0, 0, 1, -1, -1]
    branch_left_3 = [0, 1, -1, -1, -1]
    branch_left_4 = [0, 2, -1, -1, -1]
    branch_left_5 = [0, 3, -1, -1, -1]
    branch_left_6 = [1, -1, -1, -1, -1]
    branch_left_7 = [2, -1, -1, -1, -1]
    branch_left_8 = [-1, -1, -1, -1, -1]

    tree_str_left = '00, 10, 20, 30, 40'


    #Test
    method_name = '__str__'
    test_cod = 1
    input_1 = tree
    inputs = [input_1]
    expected = tree_str_left
    test.run_test(method_name, test_cod, inputs, expected)

    set_method = 'update_branch_left'
    get_method = 'get_branch'
    test_cod = 1
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_0
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    method_name = 'get_branch_value'
    test_cod = 1
    input_1 = tree
    inputs = [input_1]
    expected = ['00', '10', '20', '30', '40']
    test.run_test(method_name, test_cod, inputs, expected)

    set_method = 'update_branch_value'
    get_method = 'get_branch_value'
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = ['00', '10', '20', '30', '40']
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'update_ls_len'
    get_method = '__len__'
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = tree_len
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    #Right empty
    set_method = 'set_branch_right_empty'
    get_method = 'get_branch'
    test_cod = 1
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_1
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch_right_empty'
    get_method = 'get_branch'
    test_cod = 2
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch_right_empty'
    get_method = 'get_branch'
    test_cod = 3
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_3
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch_right_empty'
    get_method = 'get_branch'
    test_cod = 4
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_4
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch_right_empty'
    get_method = 'get_branch'
    test_cod = 5
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_5
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch_right_empty'
    get_method = 'get_branch'
    test_cod = 6
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_6
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch_right_empty'
    get_method = 'get_branch'
    test_cod = 7
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_7
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch_right_empty'
    get_method = 'get_branch'
    test_cod = 8
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_8
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch_right_empty'
    get_method = 'get_branch'
    test_cod = 9
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_8
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch'
    get_method = 'get_branch'
    test_cod = 1
    input_1 = tree
    input_2 = [1, -1, 0, -1, -1]
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = [1, -1, 0, -1, -1]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch_right_empty'
    get_method = 'get_branch'
    test_cod = 10
    input_1 = tree
    set_inputs = [input_1]#[1, -1, 0, -1, -1]
    get_inputs = [input_1]
    expected = [1, -1, 1, -1, -1]#[3, 4, 2, 1, 2]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch'
    get_method = 'get_branch'
    test_cod = 2
    input_1 = tree
    input_2 = [1, 2, 1, 0, -1]
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = [1, 2, 1, 0, -1]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch_right_empty'
    get_method = 'get_branch'
    test_cod = 11
    input_1 = tree
    set_inputs = [input_1]#[1, 2, 1, 0, -1]
    get_inputs = [input_1]
    expected = [1, 3, -1, -1, -1]#[3, 4, 2, 1, 2]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_branch_right'
    get_method = 'get_branch'
    input_1 = tree
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [1, 3, -1, -1, 0]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    method_name = 'in_tree'
    test_cod = 1
    input_1 = tree
    inputs = [input_1]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    set_method = 'set_branch'
    get_method = 'get_branch'
    test_cod = 3
    input_1 = tree
    input_2 = [-1, 0, 0, 0, 0]
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = input_2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    method_name = 'in_tree'
    test_cod = 1
    input_1 = tree
    inputs = [input_1]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)


    #----------------------------------------------------------------#
    #----------------------------------------------------------------#
    
    print(test.get_info())
