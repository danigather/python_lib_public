from test import *
from node import *

if __name__ == '__main__':
    toTest = Node
    test = Test(toTest)

    test.append_method_noTest('append_in_leaves_compatible_deprecated')
    test.append_method_noTest('append_in_leaves_compatible')
    
    node = Node([], 'Ignacio', [])
    node0 = Node([0], 'Ignacio', [])
    node1 = Node([1], 'Cesar', [])
    node00 = Node([0, 0], 'Nacho', [])
    node01 = Node([0, 1], 'Hector', [])
    node10 = Node([1, 0], 'Daniel', [])
    node11 = Node([1, 1], 'Sonia', [])
    node12 = Node([1, 2], 'Ana', [])
    node010 = Node([0, 1, 0], 'Gael', [])

    children = ['Ignacio', 'Cesar']
    children0 = ['Nacho', 'Hector']
    children1 = ['Daniel', 'Sonia', 'Ana']
    children01 = ['Gael']

    nodes_children = [node0, node1]
    nodes_children0 = [node00, node01]
    nodes_children1 = [node10, node11, node12]
    nodes_children01 = [node010]

    len_node010 = Node([0, 1, 0], 4, [])
    len_node10 = Node([1, 0], 6, [])
    len_node11 = Node([1, 1], 5, [])
    len_node12 = Node([1, 2], 3, [])
    len_node00 = Node([0, 0], 5, [])
    len_node01 = Node([0, 1], 5, [len_node010])
    len_node0 = Node([0], 7, [len_node00, len_node01])
    len_node1 = Node([1], 5, [len_node10, len_node11, len_node12])
    len_node = Node([], 7, [len_node0, len_node1])

    len_node_str = ''
    len_node_str = len_node_str + '7' + '\n'
    len_node_str = len_node_str + '|---> 7' + '\n'
    len_node_str = len_node_str + '|  |---> 5' + '\n'
    len_node_str = len_node_str + '|  |---> 5' + '\n'
    len_node_str = len_node_str + '|     |---> 4' + '\n'
    len_node_str = len_node_str + '|---> 5' + '\n'
    len_node_str = len_node_str + '   |---> 6' + '\n'
    len_node_str = len_node_str + '   |---> 5' + '\n'
    len_node_str = len_node_str + '   |---> 3' + '\n'

    len_node1_str = ''
    len_node1_str = len_node1_str + '5' + '\n'
    len_node1_str = len_node1_str + '|---> 6' + '\n'
    len_node1_str = len_node1_str + '|---> 5' + '\n'
    len_node1_str = len_node1_str + '|---> 3' + '\n'

    sum_node010 = Node([0, 1, 0], 24, [])
    sum_node10 = Node([1, 0], 20, [])
    sum_node11 = Node([1, 1], 18, [])
    sum_node12 = Node([1, 2], 17, [])
    sum_node00 = Node([0, 0], 17, [])
    sum_node01 = Node([0, 1], 17, [sum_node010])
    sum_node0 = Node([0], 14, [sum_node00, sum_node01])
    sum_node1 = Node([1], 12, [sum_node10, sum_node11, sum_node12])
    sum_node = Node([], 7, [sum_node0, sum_node1])


    #Test
    method_name = '__str__'
    test_cod = 1
    input_1 = node
    inputs = [input_1]
    expected = 'Ignacio\n'
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__str__'
    test_cod = 2
    input_1 = len_node
    inputs = [input_1]
    expected = len_node_str
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__str__'
    test_cod = 3
    input_1 = len_node1
    inputs = [input_1]
    expected = len_node1_str
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__len__'
    test_cod = 1
    input_1 = sum_node
    inputs = [input_1]
    expected = 9
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__len__'
    test_cod = 2
    input_1 = sum_node01
    inputs = [input_1]
    expected = 2
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_value'
    test_cod = 1
    input_1 = node
    inputs = [input_1]
    expected = 'Ignacio'
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_value'
    test_cod = 2
    input_1 = node1
    inputs = [input_1]
    expected = 'Cesar'
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_value'
    test_cod = 3
    input_1 = len_node11
    inputs = [input_1]
    expected = 5
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_leave'
    test_cod = 1
    input_1 = len_node
    inputs = [input_1]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_leave'
    test_cod = 2
    input_1 = len_node0
    inputs = [input_1]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_leave'
    test_cod = 3
    input_1 = len_node10
    inputs = [input_1]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_root'
    test_cod = 1
    input_1 = len_node0
    inputs = [input_1]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_root'
    test_cod = 2
    input_1 = len_node010
    inputs = [input_1]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_root'
    test_cod = 3
    input_1 = len_node
    inputs = [input_1]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)
    method_name = '__eq__'
    test_cod = 1
    input_1 = len_node
    input_2 = len_node1
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__eq__'
    test_cod = 2
    input_1 = len_node0
    input_2 = len_node1
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__eq__'
    test_cod = 3
    input_1 = len_node
    input_2 = sum_node
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    set_method = 'add_children'
    get_method = 'get_children'
    test_cod = 1
    input_1 = node
    input_2 = children
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = nodes_children
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'add_children'
    get_method = 'get_children'
    test_cod = 2
    input_1 = node.children[0]
    input_2 = children0
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = nodes_children0
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'add_children'
    get_method = 'get_children'
    test_cod = 3
    input_1 = node.children[1]
    input_2 = children1
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = nodes_children1
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'add_child'
    get_method = 'get_children'
    test_cod = 1
    input_1 = node.children[0].children[1]
    input_2 = children01[0]
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = nodes_children01
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    method_name = 'map'
    test_cod = 1
    input_1 = node
    input_2 = len
    inputs = [input_1, input_2]
    expected = len_node
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'map_acum'
    test_cod = 1
    input_1 = len_node
    input_2 = sum
    input_3 = 0
    inputs = [input_1, input_2, input_3]
    expected = sum_node
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_ls_structure'
    test_cod = 1
    input_1 = len_node
    inputs = [input_1]
    expected = [[], [0], [0, 0], [0, 1], [0, 1, 0], [1], [1, 0], [1, 1], [1, 2]]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'to_list'
    test_cod = 1
    input_1 = node.children[0]
    inputs = [input_1]
    expected = [node0, node00, node01, node010]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'to_list'
    test_cod = 2
    input_1 = node.children[1]
    inputs = [input_1]
    expected = [node1, node10, node11, node12]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_leaves'
    test_cod = 1
    input_1 = node
    inputs = [input_1]
    expected = [node00, node010, node10, node11, node12]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_element'
    test_cod = 1
    input_1 = node.children[0]
    input_2 = [0, 1]
    inputs = [input_1, input_2]
    expected = node01
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_element'
    test_cod = 2
    input_1 = node.children[0]
    input_2 = [0, 1, 0]
    inputs = [input_1, input_2]
    expected = node010
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_branch'
    test_cod = 1
    input_1 = node
    input_2 = [0, 1, 0]
    inputs = [input_1, input_2]
    expected = [node, node0, node01, node010]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_branch'
    test_cod = 2
    input_1 = node.children[0]
    input_2 = [0, 1, 0]
    inputs = [input_1, input_2]
    expected = [node0, node01, node010]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_ls_len_children'
    test_cod = 1
    input_1 = node
    input_2 = [0, 1, 0]
    inputs = [input_1, input_2]
    expected = [2, 2, 1]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_bigger'
    test_cod = 1
    input_1 = sum_node
    inputs = [input_1]
    expected = sum_node010
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_bigger'
    test_cod = 2
    input_1 = len_node
    inputs = [input_1]
    expected = len_node10
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_bigger'
    test_cod = 3
    input_1 = node
    inputs = [input_1]
    expected = node11
    test.run_test(method_name, test_cod, inputs, expected)


    #----------------------------------------------------------------#
    #----------------------------------------------------------------#
    
    print(test.get_info())
