from test import *
from auction.sandholmDynamic import *

if __name__ == '__main__':
    toTest = SandholmDynamic
    test = Test(toTest)
    item1 = 'I1'
    item2 = 'I2'
    item3 = 'I3'
    item4 = 'I4'
    item5 = 'I5'

    ls_items = [item1, item2, item3, item4, item5]
    
    ls_item1 = [item1]
    ls_item2 = [item2]
    ls_item3 = [item3]
    ls_item4 = [item4]
    ls_item5 = [item5]

    ls_item12 = [item1, item2]
    ls_item135 = [item1, item3, item5]
    ls_item14 = [item1, item4]

    ls_item25 = [item2, item5]
    ls_item35 = [item3, item5]

    bidder0 = 'B_'
    bidder1 = 'B1'

    ls_bidders = [bidder1]

    bid10000 = Bid('DID_0', bidder0, ls_item1, 0)
    
    bid11000 = Bid('ID1', bidder1, ls_item12, 2)
    bid10101 = Bid('ID2', bidder1, ls_item135, 4)
    bid10010 = Bid('ID3', bidder1, ls_item14, 5)
    
    bid01000 = Bid('ID4', bidder1, ls_item2, 1)
    bid01001 = Bid('ID5', bidder1, ls_item25, 2)

    bid00100 = Bid('ID6', bidder1, ls_item3, 1)
    bid00101 = Bid('ID7', bidder1, ls_item35, 4)
    
    bid00010 = Bid('ID8', bidder1, ls_item4, 1)	
    bid00001 = Bid('ID9', bidder1, ls_item5, 4)
    
    
    ls_bid = [bid11000, bid10101, bid10010, \
              bid01000, bid01001, \
              bid00100, bid00101, \
              bid00010, bid00001]

    bid_dummy = bid10000

    ls_winner_bid = [bid10010, bid01000, bid00100, bid00001]
    
    
    sandholm = SandholmDynamic(ls_bid)
    sandholm1 = SandholmDynamic(ls_bid)

    tree_ls_len = [4, 2, 2, 1, 1]
    branch_left_0 = [0, 0, 0, 0, 0]#7
    branch_left_1 = [0, 0, 1, 0, -1]#6
    branch_left_2 = [0, 1, 0, 0, -1]#4
    branch_left_3 = [1, -1, 0, 0, 0]#8
    branch_left_4 = [1, -1, 1, 0, -1]#7
    branch_left_5 = [2, 0, -1, 0, -1]#6
    branch_left_6 = [3, 0, 0, -1, 0]#11
    branch_left_7 = [3, 0, 1, -1, -1]#10
    branch_left_8 = [3, 1, 0, -1, -1]#8
    branch_left_9 = [-1, 0, 0, 0, 0] 

    branch_value_left_0 = [bid10000, bid01000, bid00100, bid00010, bid00001]

    node_str = ''#[4, 2, 2, 1, 1]
    node_str = node_str + '|---> DID_0|B_|[I1]|0\n'
    node_str = node_str + '|  |---> ID4|B1|[I2]|1\n'
    node_str = node_str + '|  |  |---> ID6|B1|[I3]|1\n'
    node_str = node_str + '|  |  |  |---> ID8|B1|[I4]|1\n'
    node_str = node_str + '|  |  |     |---> ID9|B1|[I5]|4\n'#0[0, 0, 0, 0, 0]
    node_str = node_str + '|  |  |---> ID7|B1|[I3,I5]|4\n'
    node_str = node_str + '|  |     |---> ID8|B1|[I4]|1\n'#1[0, 0, 1, 0, -1]
    node_str = node_str + '|  |---> ID5|B1|[I2,I5]|2\n'
    node_str = node_str + '|     |---> ID6|B1|[I3]|1\n'
    node_str = node_str + '|        |---> ID8|B1|[I4]|1\n'#2[0, 1, 0, 0, -1]
    node_str = node_str + '|---> ID1|B1|[I1,I2]|2\n'
    node_str = node_str + '|  |---> ID6|B1|[I3]|1\n'
    node_str = node_str + '|  |  |---> ID8|B1|[I4]|1\n'
    node_str = node_str + '|  |     |---> ID9|B1|[I5]|4\n'#3[1, -1, 0, 0, 0]
    node_str = node_str + '|  |---> ID7|B1|[I3,I5]|4\n'
    node_str = node_str + '|     |---> ID8|B1|[I4]|1\n'#4[1, -1, 1, 0, -1]
    node_str = node_str + '|---> ID2|B1|[I1,I3,I5]|4\n'
    node_str = node_str + '|  |---> ID4|B1|[I2]|1\n'
    node_str = node_str + '|     |---> ID8|B1|[I4]|1\n'#5[2, 0, -1, 0, -1]
    node_str = node_str + '|---> ID3|B1|[I1,I4]|5\n'
    node_str = node_str + '   |---> ID4|B1|[I2]|1\n'
    node_str = node_str + '   |  |---> ID6|B1|[I3]|1\n'
    node_str = node_str + '   |  |  |---> ID9|B1|[I5]|4\n'#6[3, 0, 0, -1, 0]
    node_str = node_str + '   |  |---> ID7|B1|[I3,I5]|4\n'#7[3, 0, 1, -1, -1]
    node_str = node_str + '   |---> ID5|B1|[I2,I5]|2\n'
    node_str = node_str + '      |---> ID6|B1|[I3]|1\n'#8[3, 1, 0, -1, -1]

    sandholm_str = '11->[3, 0, 0, -1, 0]'

    #Test

    set_method = 'update_branch_left_compat'
    get_method = 'get_branch'
    test_cod = 1
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_0
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    method_name = 'get_branch_value'
    test_cod = 1
    input_1 = sandholm
    inputs = [input_1]
    expected = branch_value_left_0
    test.run_test(method_name, test_cod, inputs, expected)

    set_method = 'explore_right'
    get_method = 'get_branch'
    test_cod = 1
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_1
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'explore_right'
    get_method = 'get_branch'
    test_cod = 2
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'explore_right'
    get_method = 'get_branch'
    test_cod = 3
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_3
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'explore_right'
    get_method = 'get_branch'
    test_cod = 4
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_4
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'explore_right'
    get_method = 'get_branch'
    test_cod = 5
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_5
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'explore_right'
    get_method = 'get_branch'
    test_cod = 6
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_6
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'explore_right'
    get_method = 'get_branch'
    test_cod = 7
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_7
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'explore_right'
    get_method = 'get_branch'
    test_cod = 8
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_8
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'explore_right'
    get_method = 'get_branch'
    test_cod = 9
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = branch_left_9
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    method_name = '__str__'
    test_cod = 1
    input_1 = sandholm
    inputs = [input_1]
    expected = sandholm_str
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'get_profit'
    test_cod = 1
    input_1 = sandholm
    inputs = [input_1]
    expected = 11
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_profit'
    test_cod = 2
    input_1 = sandholm1
    inputs = [input_1]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_solution'
    test_cod = 1
    input_1 = sandholm
    inputs = [input_1]
    expected = ls_winner_bid
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_solution'
    test_cod = 2
    input_1 = sandholm1
    inputs = [input_1]
    expected = []
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_total_branch'
    test_cod = 1
    input_1 = sandholm
    inputs = [input_1]
    expected = 9
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_total_branch'
    test_cod = 2
    input_1 = sandholm1
    inputs = [input_1]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)

    set_method = 'solve'
    get_method = 'get_profit'
    test_cod = 1
    input_1 = sandholm1
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = 11
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    method_name = 'get_total_branch'
    test_cod = 3
    input_1 = sandholm1
    inputs = [input_1]
    expected = 9
    test.run_test(method_name, test_cod, inputs, expected)



    #----------------------------------------------------------------#
    #----------------------------------------------------------------#

    print(test.get_info())
