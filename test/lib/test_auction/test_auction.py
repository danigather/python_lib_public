from test import *
from auction.auction import *

if __name__ == '__main__':
    toTest = Auction
    test = Test(toTest)

    test.append_method_noTest('load')
    test.append_method_noTest('save')

    item1 = 'I1'
    item2 = 'I2'
    item3 = 'I3'
    item4 = 'I4'

    ls_items = [item1, item2, item3, item4]
    
    ls_item1 = [item1]
    ls_item2 = [item1, item2]
    ls_item3 = [item2, item3]
    ls_item4 = [item2, item3, item4]
    ls_item5 = [item3]
    ls_item6 = [item3, item4]
    ls_item7 = [item4]
    
    bidder1 = 'B1'
    bidder2 = 'B2'
    bidder3 = 'B3'
    bidder4 = 'B4'
    bidder5 = 'B5'

    ls_bidders = [bidder1, bidder2, bidder3, bidder4, bidder5]

    bid1 = Bid('ID1', bidder1, ls_item1, 2)
    bid2 = Bid('ID2', bidder1, ls_item2, 3)
    bid3 = Bid('ID3', bidder2, ls_item3, 5)
    bid4 = Bid('ID4', bidder2, ls_item4, 6)
    bid5 = Bid('ID5', bidder3, ls_item5, 1)
    bid6 = Bid('ID6', bidder4, ls_item6, 4)
    bid6_n = Bid('ID6_n', bidder4, ls_item6, 3)
    bid7 = Bid('ID7', bidder5, ls_item7, 1)


    ls_bid1 = [bid3, bid6_n, bid6, bid7, bid4, bid2, bid1, bid5]
    ls_bid1_2 = [bid1, bid2, bid3, bid4, bid5, bid6, bid6_n, bid7]
    ls_bid1_3 = [bid1, bid2, bid3, bid4, bid5, bid6, bid7]
    
    ls_bid2 = [bid1, bid2, bid3, bid4, bid5, bid6, bid7]

    dummy_bidder = 'B_'
    dummy_bid1 = Bid('DID_0', dummy_bidder, [item1], 0)
    dummy_bid2 = Bid('DID_1', dummy_bidder, [item2], 0)
    dummy_bid3 = Bid('DID_2', dummy_bidder, [item3], 0)
    dummy_bid4 = Bid('DID_3', dummy_bidder, [item4], 0)
    
    ls_dummy_bid = [dummy_bid1, dummy_bid2, dummy_bid3, dummy_bid4]

    str_auction2 = \
        'ID1|B1|[I1]|2\n' + \
        'ID2|B1|[I1,I2]|3\n' + \
        'ID3|B2|[I2,I3]|5\n' + \
        'ID4|B2|[I2,I3,I4]|6\n' + \
        'ID5|B3|[I3]|1\n' + \
        'ID6|B4|[I3,I4]|4\n' + \
        'ID7|B5|[I4]|1\n'

    auction1 = Auction(ls_bid1, False)
    auction1_2 = Auction(ls_bid1_2, True)

    auction2 = Auction(ls_bid2, False)

    auction_initialize = Auction([])
    
    
    #Test
    
    method_name = '__eq__'
    test_cod = 1
    input_1 = auction1
    input_2 = auction1
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__eq__'
    test_cod = 2
    input_1 = auction1
    input_2 = auction1.clone()
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = '__eq__'
    test_cod = 2
    input_1 = auction1
    input_2 = auction1_2
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__len__'
    test_cod = 1
    input_1 = auction1
    inputs = [input_1]
    expected = 7
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__len__'
    test_cod = 2
    input_1 = auction1_2
    inputs = [input_1]
    expected = 8
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = '__str__'
    test_cod = 1
    input_1 = auction2
    inputs = [input_1]
    expected = str_auction2
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'bid_compat'
    test_cod = 1
    input_1 = auction1
    input_2 = bid1
    input_3 = bid2
    inputs = [input_1, input_2, input_3]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'bid_compat'
    test_cod = 2
    input_1 = auction1
    input_2 = bid1
    input_3 = bid1
    inputs = [input_1, input_2, input_3]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'bid_compat'
    test_cod = 3
    input_1 = auction1
    input_2 = bid2
    input_3 = bid6
    inputs = [input_1, input_2, input_3]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'get_id'
    test_cod = 1
    input_1 = auction1
    inputs = [input_1]
    expected = ['ID1', 'ID2', 'ID3', 'ID4', 'ID5', 'ID6', 'ID7']
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_id'
    test_cod = 2
    input_1 = auction1_2
    inputs = [input_1]
    expected = ['ID1', 'ID2', 'DID_1', 'ID3', 'ID4', 'ID5', 'ID6', 'ID7']
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'get_id'
    test_cod = 3
    input_1 = auction2
    inputs = [input_1]
    expected = ['ID1', 'ID2', 'ID3', 'ID4', 'ID5', 'ID6', 'ID7']
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_bidders'
    test_cod = 1
    input_1 = auction1
    inputs = [input_1]
    expected = [bidder1, bidder2, bidder3, bidder4, bidder5]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_bidders'
    test_cod = 2
    input_1 = auction2
    inputs = [input_1]
    expected = ls_bidders
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_total_bidders'
    test_cod = 1
    input_1 = auction2
    inputs = [input_1]
    expected = 5
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_items'
    test_cod = 1
    input_1 = auction1
    inputs = [input_1]
    expected = ls_items
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_total_items'
    test_cod = 1
    input_1 = auction1
    inputs = [input_1]
    expected = 4
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'get_bids'
    test_cod = 1
    input_1 = auction1
    inputs = [input_1]
    expected = ls_bid2
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_bids'
    test_cod = 2
    input_1 = auction2
    inputs = [input_1]
    expected = ls_bid2
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_bids_from_bidder'
    test_cod = 1
    input_1 = auction2
    input_2 = bidder1
    inputs = [input_1, input_2]
    expected = [bid1, bid2]
    test.run_test(method_name, test_cod, inputs, expected)

    set_method = 'sort_ls_bid'
    get_method = 'get_bids'
    test_cod = 1
    input_1 = auction1.clone()
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = ls_bid2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'update_dummy'
    get_method = 'get_dummy_bids'
    test_cod = 1
    input_1 = auction1
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = []
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'update_dummy'
    get_method = 'get_dummy_bids'
    test_cod = 2
    input_1 = auction1_2
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = ls_dummy_bid
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'pre'
    get_method = 'get_bids'
    test_cod = 1
    input_1 = auction1.clone()
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = ls_bid2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    method_name = 'clone'
    test_cod = 1
    input_1 = auction1
    inputs = [input_1]
    expected = auction1
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'clone'
    test_cod = 2
    input_1 = auction1
    inputs = [input_1]
    expected = auction1_2
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_dict_compat'
    function = len
    test_cod = 1
    input_1 = auction1_2
    inputs = [input_1]
    expected = 9
    test.run_test(method_name, test_cod, inputs, expected, function)
    
    set_method = 'set_bids'
    get_method = 'get_bids'
    test_cod = 1
    input_1 = auction1.clone()
    input_2 = [bid6, bid1]
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = input_2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'update_bidders'
    get_method = 'get_bidders'
    test_cod = 1
    input_1 = auction1.clone()
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = ls_bidders
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'update_items'
    get_method = 'get_items'
    test_cod = 1
    input_1 = auction1.clone()
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = ls_items
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_bidders'
    get_method = 'get_bidders'
    test_cod = 1
    input_1 = auction_initialize
    input_2 = ['B0', 'B1', 'B2']
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = input_2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_bidders'
    get_method = 'get_bidders'
    test_cod = 2
    input_1 = auction_initialize
    input_2 = ['B00', 'B01', 'B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B09']
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = input_2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_items'
    get_method = 'get_items'
    test_cod = 2
    input_1 = auction_initialize
    input_2 = ['I0', 'I1', 'I2']
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = input_2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_items'
    get_method = 'get_items'
    test_cod = 2
    input_1 = auction_initialize
    input_2 = ['I00', 'I01', 'I02', 'I03', 'I04', 'I05', 'I06', 'I07', 'I08', 'I09']
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = input_2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'update_dict_compat'
    get_method = 'get_dict_compat'
    test_cod = 1
    input_1 = auction_initialize
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = {'DID_': {'DID_': True}}
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    method_name = 'initialize'
    test_cod = 1
    input_1 = 10
    input_2 = 5
    inputs = [input_1, input_2]
    expected = auction_initialize
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'append_random_bids'
    function = len
    test_cod = 1
    input_1 = auction_initialize
    input_2 = 10
    input_3 = 2
    input_4 = 3
    inputs = [input_1, input_2, input_3, input_4]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected, function)

    method_name = 'append_winner_bids'
    function = len
    test_cod = 1
    input_1 = auction_initialize
    input_2 = 2
    input_3 = 3
    inputs = [input_1, input_2, input_3]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected, function)
    
    #----------------------------------------------------------------#
    #----------------------------------------------------------------#

    print(test.get_info())


