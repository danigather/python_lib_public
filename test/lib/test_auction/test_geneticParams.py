from test import *
from auction.geneticParams import *

if __name__ == '__main__':
    toTest = GeneticParams
    test = Test(toTest)

    geneticParams = GeneticParams()

    #Test
    method_name = 'get_params_selection'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = 25
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_params_crossover'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = (50, 2)
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_params_mutation'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = (25, 0.2)
    test.run_test(method_name, test_cod, inputs, expected)
    
    set_method = 'set_params_selection'
    get_method = 'get_params_selection'
    test_cod = 1
    input_1 = geneticParams
    input_2 = 10
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = 10
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_params_crossover'
    get_method = 'get_params_crossover'
    test_cod = 1
    input_1 = geneticParams
    input_2 = 10
    input_3 = 3
    set_inputs = [input_1, input_2, input_3]
    get_inputs = [input_1]
    expected = (10, 3)
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'set_params_mutation'
    get_method = 'get_params_mutation'
    test_cod = 1
    input_1 = geneticParams
    input_2 = 10
    input_3 = 0.5
    set_inputs = [input_1, input_2, input_3]
    get_inputs = [input_1]
    expected = (10, 0.5)
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    method_name = 'initialize'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = geneticParams
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'fitness'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = geneticParams
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'selection_league'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = geneticParams
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'selection_cup'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = geneticParams
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'selection'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = geneticParams
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'crossover'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = geneticParams
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'mutation'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = geneticParams
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_total_cicles'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_profit_cicle'
    test_cod = 1
    input_1 = geneticParams
    inputs = [input_1]
    expected = []
    test.run_test(method_name, test_cod, inputs, expected)

    test.append_method_noTest('iterate')
    test.append_method_noTest('get_profit')
    test.append_method_noTest('cicle')
    test.append_method_noTest('solve')
    test.append_method_noTest('get_solution')

    
    #----------------------------------------------------------------#
    #----------------------------------------------------------------#

    print(test.get_info())

