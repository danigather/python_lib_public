from test import *
from auction.bid import *

if __name__ == '__main__':
    toTest = Bid
    test = Test(toTest)
    
    item1 = 'I1'
    item2 = 'I2'
    item3 = 'I3'
    item4 = 'I4'

    ls_item1 = [item1, item2]
    ls_item1_2 = [item2, item1]    
    ls_item2 = [item3, item4]
    ls_item3 = [item2, item3]
    ls_item4 = [item3]
    ls_item5 = [item4]
    ls_item6 = [item2, item3, item4]
    ls_item7 = [item1]
    ls_item8 = [item1, item2, item3]
    
    bidder1 = 'B1'
    bidder2 = 'B2'
    
    bid1 = Bid('ID1', bidder1, ls_item1, 3)
    bid1_2 = Bid('ID1', bidder1, ls_item1_2, 4)

    bid1 = bid1.clone()
    bid2 = Bid('ID2', bidder1, ls_item2, 4)
    bid3 = Bid('ID3', bidder1, ls_item3, 5)
    bid4 = Bid('ID4', bidder1, ls_item4, 1)
    bid5 = Bid('ID5', bidder1, ls_item5, 1)
    bid6 = Bid('ID6', bidder1, ls_item6, 5)
    bid7 = Bid('ID7', bidder1, ls_item7, 3)
    bid8 = Bid('ID8', bidder1, ls_item8, 3)

    bid_b2_1 = Bid('ID1', bidder2, ls_item1, 3)

    ls_bid = [bid1, bid2, bid3, bid4, bid5, bid6, bid7, bid8]
    ls_bid1 = [bid2, bid4]
    ls_bid2 = [bid2, bid3]

    dict_compat_ID1 = {'ID1':False}
    dict_compat_ID1.update({'ID2':True})
    dict_compat_ID1.update({'ID3':False})
    dict_compat_ID1.update({'ID4':True})
    dict_compat_ID1.update({'ID5':True})
    dict_compat_ID1.update({'ID6':False})
    dict_compat_ID1.update({'ID7':False})
    dict_compat_ID1.update({'ID8':False})
    dict_compat = {'ID1':dict_compat_ID1}
  
    #Test
    method_name = 'empty'
    test_cod = 1
    inputs = []
    expected = Bid('DID_', 'B_', [], 0)
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = '__eq__'
    test_cod = 1
    input_1 = bid1
    input_2 = bid1
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__eq__'
    test_cod = 2
    input_1 = bid1
    input_2 = bid2
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__eq__'
    test_cod = 3
    input_1 = bid1
    input_2 = bid1_2
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__lt__'
    test_cod = 1
    input_1 = bid1
    input_2 = bid1
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__lt__'
    test_cod = 2
    input_1 = bid1_2
    input_2 = bid1
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__lt__'
    test_cod = 3
    input_1 = bid1
    input_2 = bid1_2
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__lt__'
    test_cod = 4
    input_1 = bid1
    input_2 = bid_b2_1
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__lt__'
    test_cod = 5
    input_1 = bid_b2_1
    input_2 = bid1
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__lt__'
    test_cod = 6
    input_1 = bid1
    input_2 = bid6
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__lt__'
    test_cod = 7
    input_1 = bid6
    input_2 = bid1
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = '__lt__'
    test_cod = 8
    input_1 = bid1
    input_2 = bid8
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__lt__'
    test_cod = 9
    input_1 = bid8
    input_2 = bid1
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__lt__'
    test_cod = 10
    input_1 = bid1
    input_2 = bid7
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__lt__'
    test_cod = 11
    input_1 = bid7
    input_2 = bid1
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_worse'
    test_cod = 1
    input_1 = bid1
    input_2 = bid1
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_worse'
    test_cod = 2
    input_1 = bid1_2
    input_2 = bid1
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_worse'
    test_cod = 3
    input_1 = bid1
    input_2 = bid1_2
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_worse'
    test_cod = 4
    input_1 = bid1
    input_2 = bid8
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_worse'
    test_cod = 5
    input_1 = bid8
    input_2 = bid1
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)
        
    method_name = '__str__'
    test_cod = 1
    input_1 = bid1
    inputs = [input_1]
    expected = 'ID1|B1|[I1,I2]|3'
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = '__len__'
    test_cod = 1
    input_1 = bid1
    inputs = [input_1]
    expected = 2
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'disjoint'
    test_cod = 1
    input_1 = bid1
    input_2 = bid8
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'disjoint'
    test_cod = 2
    input_1 = bid8
    input_2 = bid1
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'get_id'
    test_cod = 1
    input_1 = bid1
    inputs = [input_1]
    expected = 'ID1'
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_id'
    test_cod = 1
    input_1 = bid2
    inputs = [input_1]
    expected = 'ID2'
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_bidder'
    test_cod = 1
    input_1 = bid2
    inputs = [input_1]
    expected = 'B1'
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_items'
    test_cod = 1
    input_1 = bid1
    inputs = [input_1]
    expected = [item1, item2]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_score'
    test_cod = 1
    input_1 = bid1
    inputs = [input_1]
    expected = 3
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_score'
    test_cod = 1
    input_1 = bid2
    inputs = [input_1]
    expected = 4
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'clone'
    test_cod = 1
    input_1 = bid1
    inputs = [input_1]
    expected = bid1
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_contained'
    test_cod = 1
    input_1 = bid1
    input_2 = ls_item1
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'is_contained'
    test_cod = 2
    input_1 = bid1
    input_2 = ls_item7
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'is_contained'
    test_cod = 3
    input_1 = bid1
    input_2 = ls_item6
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'is_contained'
    test_cod = 4
    input_1 = bid1
    input_2 = ls_item8
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'get_score_of_items'
    test_cod = 1
    input_1 = bid1
    input_2 = ls_item1
    inputs = [input_1, input_2]
    expected = (3, [])
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'get_score_of_items'
    test_cod = 2
    input_1 = bid1
    input_2 = ls_item8
    inputs = [input_1, input_2]
    expected = (3, [item3])
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'get_score_of_items'
    test_cod = 3
    input_1 = bid1
    input_2 = ls_item6
    inputs = [input_1, input_2]
    expected = (0, ls_item6)
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'is_compatible_with'
    test_cod = 1
    input_1 = bid1
    input_2 = ls_bid1
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_compatible_with'
    test_cod = 1
    input_1 = bid1
    input_2 = ls_bid2
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_compat'
    test_cod = 1
    input_1 = bid1
    input_2 = bid1
    input_3 = dict_compat
    inputs = [input_1, input_2, input_3]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_compat'
    test_cod = 2
    input_1 = bid1
    input_2 = bid2
    input_3 = dict_compat
    inputs = [input_1, input_2, input_3]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_compat'
    test_cod = 3
    input_1 = bid1
    input_2 = bid3
    input_3 = dict_compat
    inputs = [input_1, input_2, input_3]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'is_compat'
    test_cod = 3
    input_1 = bid1
    input_2 = bid4
    input_3 = dict_compat
    inputs = [input_1, input_2, input_3]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'get_compatible'
    test_cod = 1
    input_1 = bid1
    input_2 = ls_bid
    input_3 = dict_compat
    inputs = [input_1, input_2, input_3]
    expected = [bid2, bid4, bid5]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'sum_score'
    test_cod = 1
    input_1 = bid1
    input_2 = bid2
    inputs = [input_1, input_2]
    expected = 7
    test.run_test(method_name, test_cod, inputs, expected)

    #----------------------------------------------------------------#
    #----------------------------------------------------------------#

    print(test.get_info())


