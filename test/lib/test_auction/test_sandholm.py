from test import *
from auction.sandholm import *

if __name__ == '__main__':
    toTest = Sandholm
    test = Test(toTest)

    test.append_method_noTest('update_search1_deprecated')
    
    item1 = 'I1'
    item2 = 'I2'
    item3 = 'I3'
    item4 = 'I4'
    item5 = 'I5'

    ls_items = [item1, item2, item3, item4, item5]
    
    ls_item1 = [item1]
    ls_item2 = [item2]
    ls_item3 = [item3]
    ls_item4 = [item4]
    ls_item5 = [item5]

    ls_item12 = [item1, item2]
    ls_item135 = [item1, item3, item5]
    ls_item14 = [item1, item4]

    ls_item25 = [item2, item5]
    ls_item35 = [item3, item5]

    bidder1 = 'B1'

    ls_bidders = [bidder1]

    bid_empty = Bid('DID_', None, [], 0)

    bid11000 = Bid('ID1', bidder1, ls_item12, 2)
    bid10101 = Bid('ID2', bidder1, ls_item135, 4)
    bid10010 = Bid('ID3', bidder1, ls_item14, 5)
    
    bid01000 = Bid('ID4', bidder1, ls_item2, 1)
    bid01001 = Bid('ID5', bidder1, ls_item25, 2)

    bid00100 = Bid('ID6', bidder1, ls_item3, 1)
    bid00101 = Bid('ID7', bidder1, ls_item35, 4)
    
    bid00010 = Bid('ID8', bidder1, ls_item4, 1)	
    bid00001 = Bid('ID9', bidder1, ls_item5, 4)
    
    
    ls_bid = [bid11000, bid10101, bid10010, \
              bid01000, bid01001, \
              bid00100, bid00101, \
              bid00010, bid00001]

    sandholm = Sandholm(ls_bid)
    sandholm2 = Sandholm(ls_bid)

    node_bid_empty = Node([], bid_empty, [])
    node_profit_empty = Node([], 0, [])

    ls_winner_bid = [bid10010, bid01000, bid00100, bid00001]
    
    tree_str = ''
    tree_str = tree_str + 'DID_|B_|[]|0\n'
    tree_str = tree_str + '|---> DID_0|B_|[I1]|0\n'
    tree_str = tree_str + '|  |---> ID4|B1|[I2]|1\n'
    tree_str = tree_str + '|  |  |---> ID6|B1|[I3]|1\n'
    tree_str = tree_str + '|  |  |  |---> ID8|B1|[I4]|1\n'
    tree_str = tree_str + '|  |  |     |---> ID9|B1|[I5]|4\n'
    tree_str = tree_str + '|  |  |---> ID7|B1|[I3,I5]|4\n'
    tree_str = tree_str + '|  |     |---> ID8|B1|[I4]|1\n'
    tree_str = tree_str + '|  |---> ID5|B1|[I2,I5]|2\n'
    tree_str = tree_str + '|     |---> ID6|B1|[I3]|1\n'
    tree_str = tree_str + '|        |---> ID8|B1|[I4]|1\n'
    tree_str = tree_str + '|---> ID1|B1|[I1,I2]|2\n'
    tree_str = tree_str + '|  |---> ID6|B1|[I3]|1\n'
    tree_str = tree_str + '|  |  |---> ID8|B1|[I4]|1\n'
    tree_str = tree_str + '|  |     |---> ID9|B1|[I5]|4\n'
    tree_str = tree_str + '|  |---> ID7|B1|[I3,I5]|4\n'
    tree_str = tree_str + '|     |---> ID8|B1|[I4]|1\n'
    tree_str = tree_str + '|---> ID2|B1|[I1,I3,I5]|4\n'
    tree_str = tree_str + '|  |---> ID4|B1|[I2]|1\n'
    tree_str = tree_str + '|     |---> ID8|B1|[I4]|1\n'
    tree_str = tree_str + '|---> ID3|B1|[I1,I4]|5\n'
    tree_str = tree_str + '   |---> ID4|B1|[I2]|1\n'
    tree_str = tree_str + '   |  |---> ID6|B1|[I3]|1\n'
    tree_str = tree_str + '   |  |  |---> ID9|B1|[I5]|4\n'
    tree_str = tree_str + '   |  |---> ID7|B1|[I3,I5]|4\n'
    tree_str = tree_str + '   |---> ID5|B1|[I2,I5]|2\n'
    tree_str = tree_str + '      |---> ID6|B1|[I3]|1\n'

    #Test
    set_method = 'update_search1'
    get_method = 'get_search1'
    function = len
    test_cod = 1
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = 27
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected, function)
    
    method_name = '__str__'
    test_cod = 1
    input_1 = sandholm
    inputs = [input_1]
    expected = tree_str
    test.run_test(method_name, test_cod, inputs, expected)

    set_method = 'update_tree_profit'
    get_method = 'get_tree_profit'
    test_cod = 1
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = node_profit_empty
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'update_winner_bids'
    get_method = 'get_solution'
    test_cod = 1
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = ls_winner_bid
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    method_name = 'get_profit'
    test_cod = 1
    input_1 = sandholm
    inputs = [input_1]
    expected = 11
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_profit'
    test_cod = 2
    input_1 = sandholm2
    inputs = [input_1]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)

    set_method = 'solve'
    get_method = 'get_profit'
    test_cod = 1
    input_1 = sandholm
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = 11
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    #----------------------------------------------------------------#
    #----------------------------------------------------------------#

    print(test.get_info())

