from test import *
from auction.geneticBid import *

if __name__ == '__main__':
    toTest = GeneticBid
    test = Test(toTest)
    test.append_method_noTest('initialize')
    
    item0 = 'I0'
    item1 = 'I1'
    item2 = 'I2'
    item3 = 'I3'

    ls_item = [item0, item1, item2, item3]
    
    ls_item0 = [item0, item1]
    ls_item1 = [item2, item3]
    ls_item2 = [item1, item2]
    ls_item3 = [item2]
    ls_item4 = [item3]
    ls_item5 = [item1, item2, item3]
    ls_item6 = [item0]
    

    bidder0 = 'B0'
    bidder1 = 'B1'
    bidder2 = 'B2'
    bidder3 = 'B3'
    bidder4 = 'B4'
    bidder9 = 'B_'

    bid0 = Bid('ID0', bidder0, ls_item0, 3)
    bid1 = Bid('ID1', bidder1, ls_item1, 1)
    bid1_2 = Bid('ID1', bidder1, ls_item1, 1)
    bid1_3 = Bid('ID1', bidder1, ls_item1, 1)
    bid1_4 = Bid('ID1', bidder1, ls_item1, 1)
    
    bid2 = Bid('ID2', bidder1, ls_item1, 4)
    bid3 = Bid('ID3', bidder2, ls_item2, 5)
    bid4 = Bid('ID4', bidder2, ls_item3, 1) 
    bid5 = Bid('ID5', bidder3, ls_item4, 1)
    bid6 = Bid('ID6', bidder4, ls_item5, 4)
    bid7 = Bid('ID7', bidder4, ls_item6, 2)
    bid8 = Bid('ID9', bidder9, ls_item, 0)

    bid0_ls = []
    for i in range(100):
        bid0_ls.append(Bid(str(100+i), bidder0, ls_item0, 3))
        
    ls_bid0 = [bid0, bid1, bid2, bid3, bid4, bid5, bid6, bid7]
    ls_bid1 = ls_bid0 + bid0_ls

    distribution_bid0 = [bid0, bid2] #(bids)
                #price    3      1


    distribution_bid1 = [bid0, bid4, bid5] #(bids)
                    #price   3   1    1

    distribution_bid2 = [bid3, bid5, bid7] #(bids)
                    #price  5   1    2

    ls_solution = [distribution_bid0, distribution_bid1, distribution_bid2]

    geneticBid0 = GeneticBid(ls_bid0)

    #Test
    
    set_method = 'append_distribution_bids'
    get_method = 'get_distribution_bids'
    test_cod = 1
    input_1 = geneticBid0
    input_2 = distribution_bid0
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = [distribution_bid0]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'update_ls_score'
    get_method = 'get_ls_score'
    test_cod = 1
    input_1 = geneticBid0
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [7]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'append_distribution_bids'
    get_method = 'get_distribution_bids'
    test_cod = 2
    input_1 = geneticBid0
    input_2 = distribution_bid1
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = [distribution_bid0, distribution_bid1]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)
 
    set_method = 'update_ls_score'
    get_method = 'get_ls_score'
    test_cod = 2
    input_1 = geneticBid0
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [7, 5]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'append_distribution_bids'
    get_method = 'get_distribution_bids'
    test_cod = 3
    input_1 = geneticBid0
    input_2 = distribution_bid2
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = [distribution_bid0, distribution_bid1, distribution_bid2]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'update_ls_score'
    get_method = 'get_ls_score'
    test_cod = 3
    input_1 = geneticBid0
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [7, 5, 8]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    #Ciclo fitness - selection
    geneticBid1 = GeneticBid(ls_bid0)
    geneticBid1.set_distribution_bids(ls_solution)
    geneticBid1.set_params_selection(selection_elements = 1)

    method_name = 'get_distribution_bids'
    test_cod = 1
    input_1 = geneticBid1
    inputs = [input_1]
    expected = ls_solution
    test.run_test(method_name, test_cod, inputs, expected)

    set_method = 'fitness'
    get_method = 'get_ls_score'
    test_cod = 1
    input_1 = geneticBid1
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [7, 5, 8]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'selection'
    get_method = 'get_distribution_bids'
    test_cod = 1
    input_1 = geneticBid1
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [distribution_bid2]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)
 
    #crossover
    geneticBid1 = GeneticBid(ls_bid0)
    geneticBid1.set_distribution_bids(ls_solution)
    geneticBid1.set_params_crossover(crossover_elements=2, crossover_parents=2)

    set_method = 'crossover'
    get_method = 'get_distribution_bids'
    function = len
    test_cod = 1
    input_1 = geneticBid1
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = len(ls_solution)+2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected, function)
    
    #mutation
    geneticBid1 = GeneticBid(ls_bid0)
    geneticBid1.set_distribution_bids(ls_solution)
    geneticBid1.set_params_mutation(mutation_elements=2, mutation_index=0.3)
    
    set_method = 'mutation'
    get_method = 'get_distribution_bids'
    function = len
    test_cod = 1
    input_1 = geneticBid1
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = len(ls_solution)+2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected, function)
     
    #completeCicle
    geneticBid1 = GeneticBid(ls_bid1)
    geneticBid1.set_params_selection(2)
    geneticBid1.set_params_crossover(1, 2)
    geneticBid1.set_params_mutation(1, 0.2)
    geneticBid1.iterate()
    
    #----------------------------------------------------------------#
    #----------------------------------------------------------------#

    print(test.get_info())

