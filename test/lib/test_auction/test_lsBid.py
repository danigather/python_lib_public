from test import *
from auction.lsBid import *

if __name__ == '__main__':
    toTest = LsBid
    test = Test(toTest)
    
    obj1 = 'obj1'
    obj2 = 'obj2'
    obj3 = 'obj3'
    obj4 = 'obj4'

    obj11 = 'obj11'
    obj12 = 'obj12'
    obj13 = 'obj13'
    obj14 = 'obj14'    

    ls_obj1 = [obj1, obj2]
    ls_obj2 = [obj2, obj3]
    ls_obj3 = [obj3, obj4]

    ls_obj11_c = [obj11, obj12]
    ls_obj12_c = [obj12, obj13]
    ls_obj13_c = [obj13, obj14]
    ls_obj14_cs = [obj14, obj11]

    ls_obj11_i = [obj11, obj1]
    ls_obj12_i = [obj12, obj2]
    ls_obj13_i = [obj13, obj3]
    
    ls_obj2_d = [obj2, obj1]
    ls_obj3_d = [obj3, obj2]    
    ls_obj4_d = [obj4, obj3]

    ls_obj2_s = [obj2, obj1]
    ls_obj3_s = [obj3, obj1]    
    ls_obj4_s = [obj4, obj1]
    
    bidder1 = '0001'
    
    bid1 = Bid('1', bidder1, ls_obj1, 3)
    bid2 = Bid('2', bidder1, ls_obj2, 4)
    bid3 = Bid('3', bidder1, ls_obj3, 5)

    bid2_d = Bid('2_d', bidder1, ls_obj2_d, 3)
    bid3_d = Bid('3_d', bidder1, ls_obj3_d, 4)
    bid4_d = Bid('4_d', bidder1, ls_obj4_d, 5)

    bid2_s = Bid('2_s', bidder1, ls_obj2_s, 3)
    bid3_s = Bid('3_s', bidder1, ls_obj3_s, 4)
    bid4_s = Bid('4_s', bidder1, ls_obj4_s, 5)

    #Compatible
    bid11_c = Bid('11_c', bidder1, ls_obj11_c, 3)
    bid12_c = Bid('12_c', bidder1, ls_obj12_c, 4)
    bid13_c = Bid('13_c', bidder1, ls_obj13_c, 5)
    bid14_cs = Bid('14_cs', bidder1, ls_obj14_cs, 5)
    #Incompatible
    bid11_i = Bid('11_i', bidder1, ls_obj11_i, 3)
    bid12_i = Bid('12_i', bidder1, ls_obj12_i, 4)
    bid13_i = Bid('13_i', bidder1, ls_obj13_i, 5)
    
    

    ls_bid = [bid1, bid2, bid3, bid2_d, bid3_d, bid4_d, \
              bid11_i, bid12_i, bid13_i, \
              bid11_c, bid12_c, bid13_c, bid14_cs]

    ls_bid1 = [bid1, bid2, bid3_s]
    ls_bid1_2 = [bid2, bid1, bid3_s]
    ls_bid1_3 = [bid1, bid3_s, bid2]

    ls_bid_dict = [bid1, bid2, bid3, bid3_s, bid4_s]
    
    dict_compat_B1 = {'1':False, '2':False, '3':True, '3_s':False, '4_s':False}
    dict_compat_B2 = {'1':False, '2':False, '3':False, '3_s':False, '4_s':True}
    dict_compat_B3 = {'1':True, '2':False, '3':False, '3_s':False, '4_s':False}
    dict_compat_B3_s = {'1':False, '2':False, '3':False, '3_s':False, '4_s':False}
    dict_compat_B4_s = {'1':False, '2':True, '3':False, '3_s':False, '4_s':False}

    dict_compat = {'1':dict_compat_B1, '2':dict_compat_B2, '3':dict_compat_B3,\
                   '3_s':dict_compat_B3_s, '4_s':dict_compat_B4_s}

    ls_bid_compatible = [bid1, bid3]
        
    ls_bid2 = [bid1, bid2, bid3, bid4_s]

    ls_bid_c = [bid11_c, bid12_c, bid13_c, bid14_cs]

    ls_bid3 = [bid1, bid11_i]
    ls_bid4 = [bid2, bid13_i]

    lss_bid1_s1 = [ls_bid2, ls_bid1, ls_bid_c, bid3]
    lss_bid1_s2 = [ls_bid2, ls_bid1_2, ls_bid_c, bid3]
    lss_bid1_s3 = [ls_bid2, ls_bid_c, ls_bid3, ls_bid1_3]

    lss_bid1_n1 = [ls_bid2, ls_bid_c, ls_bid3]
    lss_bid1_n2 = [ls_bid2, ls_bid2, ls_bid_c, ls_bid3]
    lss_bid1_n3 = [ls_bid2, ls_bid_c]

    
    #Test
    method_name = 'get_random_compatible'
    function = len
    test_cod = 1
    input_1 = ls_bid1
    input_2 = dict_compat
    inputs = [input_1, input_2]
    expected = 1
    test.run_test(method_name, test_cod, inputs, expected, function)

    method_name = 'get_random_compatible'
    function = len
    test_cod = 2
    input_1 = ls_bid2
    input_2 = dict_compat
    inputs = [input_1, input_2]
    expected = 2
    test.run_test(method_name, test_cod, inputs, expected, function)

    method_name = 'eq'
    test_cod = 1
    input_1 = ls_bid1
    input_2 = ls_bid2
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'eq'
    test_cod = 1
    input_1 = ls_bid1
    input_2 = ls_bid1_2
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'extract_ls_compatible'
    test_cod = 1
    input_1 = ls_bid1
    input_2 = ls_bid
    inputs = [input_1, input_2]
    expected = ls_bid_c
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'get_complete_random_compatible'
    function = len
    test_cod = 1
    input_1 = ls_bid_compatible
    input_2 = ls_bid_dict
    input_3 = dict_compat
    inputs = [input_1, input_2, input_3]
    expected = 2
    test.run_test(method_name, test_cod, inputs, expected, function)

    method_name = 'in_ls'
    test_cod = 1
    input_1 = ls_bid1
    input_2 = lss_bid1_s1
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'in_ls'
    test_cod = 2
    input_1 = ls_bid1
    input_2 = lss_bid1_s2
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'in_ls'
    test_cod = 3
    input_1 = ls_bid1
    input_2 = lss_bid1_s3
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'in_ls'
    test_cod = 4
    input_1 = ls_bid1
    input_2 = lss_bid1_n1
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'in_ls'
    test_cod = 5
    input_1 = ls_bid1
    input_2 = lss_bid1_n2
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'in_ls'
    test_cod = 6
    input_1 = ls_bid1
    input_2 = lss_bid1_n3
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_first_compat'
    test_cod = 1
    input_1 = []
    input_2 = ls_bid_dict
    input_3 = dict_compat
    inputs = [input_1, input_2, input_3]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_first_compat'
    test_cod = 2
    input_1 = [bid1]
    input_2 = ls_bid_dict
    input_3 = dict_compat
    inputs = [input_1, input_2, input_3]
    expected = 2
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_first_compat'
    test_cod = 3
    input_1 = [bid2]
    input_2 = ls_bid_dict
    input_3 = dict_compat
    inputs = [input_1, input_2, input_3]
    expected = 4
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_first_compat'
    test_cod = 4
    input_1 = [bid3]
    input_2 = ls_bid_dict
    input_3 = dict_compat
    inputs = [input_1, input_2, input_3]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_first_compat'
    test_cod = 5
    input_1 = [bid1, bid2]
    input_2 = ls_bid_dict
    input_3 = dict_compat
    inputs = [input_1, input_2, input_3]
    expected = -1
    test.run_test(method_name, test_cod, inputs, expected)
    
    #----------------------------------------------------------------#
    #----------------------------------------------------------------#

    print(test.get_info())


