from test import *
from auction.geneticItem import *

if __name__ == '__main__':
    toTest = GeneticItem
    test = Test(toTest)
    
    item0 = 'I0'
    item1 = 'I1'
    item2 = 'I2'
    item3 = 'I3'

    ls_item = [item0, item1, item2, item3]
    
    ls_item0 = [item0, item1]
    ls_item1 = [item2, item3]
    ls_item2 = [item1, item2]
    ls_item3 = [item2]
    ls_item4 = [item3]
    ls_item5 = [item1, item2, item3]
    ls_item6 = [item0]
    

    bidder0 = 'B0'
    bidder1 = 'B1'
    bidder2 = 'B2'
    bidder3 = 'B3'
    bidder4 = 'B4'
    bidder9 = 'B_'

    bid0 = Bid('ID0', bidder0, ls_item0, 3)
    bid1 = Bid('ID1', bidder1, ls_item1, 1)
    bid1_2 = Bid('ID1', bidder1, ls_item1, 1)
    bid1_3 = Bid('ID1', bidder1, ls_item1, 1)
    bid1_4 = Bid('ID1', bidder1, ls_item1, 1)
    
    bid2 = Bid('ID2', bidder1, ls_item1, 4)
    bid3 = Bid('ID3', bidder2, ls_item2, 5)
    bid4 = Bid('ID4', bidder2, ls_item3, 1) 
    bid5 = Bid('ID5', bidder3, ls_item4, 1)
    bid6 = Bid('ID6', bidder4, ls_item5, 4)
    bid7 = Bid('ID7', bidder4, ls_item6, 2)
    bid9 = Bid('ID9', bidder9, ls_item, 0)

    ls_bid0 = [bid0, bid1, bid2, bid3, bid4, bid5, bid6, bid7, bid9]

    #               item    0  1  2  3 
    distribution_items0 = [0, 0, 2, 3] #(bidders)
                    #ls    0   3  4
                    #price   3   1  1

    #               item    0  1  2  3 
    distribution_items1 = [0, 0, 2, 4] #(bidders)
                    #ls    0   3  4
                    #price   3   1  0

    distribution_items2 = [0, 0, 2, 3] #(bidders)

    distribution_itemsW = [4, 2, 2, 3] #(bidders)

    ls_distribution_items = [distribution_items0, distribution_items1]

    geneticItem0 = GeneticItem(ls_bid0)
    geneticItem1 = GeneticItem(ls_bid0)


    #Test

    set_method = 'append_distribution_items'
    get_method = 'get_distribution_items'
    test_cod = 1
    input_1 = geneticItem0
    input_2 = distribution_items0
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = [distribution_items0]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)
    
    set_method = 'update_distribution_bidders'
    get_method = 'get_distribution_bidders'
    test_cod = 1
    input_1 = geneticItem0
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [[ls_item0, [], ls_item3, ls_item4, [], []]]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'update_ls_score'
    get_method = 'get_ls_score'
    test_cod = 1
    input_1 = geneticItem0
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [5]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)
    
    set_method = 'append_distribution_items'
    get_method = 'get_distribution_items'
    test_cod = 2
    input_1 = geneticItem1
    input_2 = distribution_items1
    set_inputs = [input_1, input_2]
    get_inputs = [input_1]
    expected = [distribution_items1]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)
    
    set_method = 'update_distribution_bidders'
    get_method = 'get_distribution_bidders'
    test_cod = 2
    input_1 = geneticItem1
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [[ls_item0, [], ls_item3, [], ls_item4, []]]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)
    
    set_method = 'update_ls_score'
    get_method = 'get_ls_score'
    test_cod = 2
    input_1 = geneticItem1
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [4]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    
    #Ciclo fitness - selection
    geneticItem2 = GeneticItem(ls_bid0)
    geneticItem2.set_distribution_items(ls_distribution_items)
    geneticItem2.set_params_selection(selection_elements = 1)

    method_name = 'get_distribution_items'
    test_cod = 1
    input_1 = geneticItem2
    inputs = [input_1]
    expected = ls_distribution_items
    test.run_test(method_name, test_cod, inputs, expected)

    set_method = 'fitness'
    get_method = 'get_ls_score'
    test_cod = 1
    input_1 = geneticItem2
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [5, 4]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    set_method = 'selection'
    get_method = 'get_distribution_items'
    test_cod = 1
    input_1 = geneticItem2
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = [distribution_items0]
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected)

    #crossover
    geneticItem2 = GeneticItem(ls_bid0)
    ls_distribution_items2=[[1, 1, 1, 1], [2, 2, 2, 2]]
    geneticItem2.set_distribution_items(ls_distribution_items2)
    geneticItem2.set_params_crossover(crossover_elements=5, crossover_parents=2)
 
    set_method = 'crossover'
    get_method = 'get_distribution_items'
    function = len
    test_cod = 1
    input_1 = geneticItem2
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = 5+2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected, function)

    #mutation
    geneticItem3 = GeneticItem(ls_bid0)
    ls_distribution_items2=[[1, 1, 1, 1], [2, 2, 2, 2]]
    geneticItem3.set_distribution_items(ls_distribution_items2)
    geneticItem3.set_params_mutation(mutation_elements=10, mutation_index=0.3)
    
    set_method = 'mutation'
    get_method = 'get_distribution_items'
    function = len
    test_cod = 1
    input_1 = geneticItem3
    set_inputs = [input_1]
    get_inputs = [input_1]
    expected = 10+2
    test.run_test_set_get(set_method, get_method, test_cod, set_inputs, get_inputs, expected, function)
    
    #completeCicle
    geneticItem4 = GeneticItem(ls_bid0)
    geneticItem4.iterate()


    
    #----------------------------------------------------------------#
    #----------------------------------------------------------------#

    print(test.get_info())

