from test import *
from MyList import *

if __name__ == '__main__':
    toTest = MyList
    test = Test(toTest)
    

    lss_1 = [['00', '01', '02', '03'],
             ['10', '11', '12', '13'],
             ['20', '21', '22', '23'],
             ['30', '31', '32', '33'],
             ['40', '41', '42', '43']]

    lss_1_t = [['00', '10', '20', '30', '40'],
               ['01', '11', '21', '31', '41'],
               ['02', '12', '22', '32', '42'],
               ['03', '13', '23', '33', '43']]

    ls_0 = []
    ls_1 = [4, 5, 2, 9, 6]
    ls_2 = [1,2,3,4]
    ls_3 = [5,6,7,8]
    ls_4 = [1,2,3,7]
    ls_4_2 = [1,7,3,2]
    ls_5 = [3]
               
    #Test
    method_name = 'select'
    test_cod = 1
    input_1 = ls_1
    input_2 = [0, 1, 2]
    inputs = [input_1, input_2]
    expected = ls_1[:3]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_inner'
    test_cod = 1
    input_1 = ls_0
    input_2 = ls_1
    inputs = [input_1, input_2]
    expected = []
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_inner'
    test_cod = 2
    input_1 = ls_1
    input_2 = ls_0
    inputs = [input_1, input_2]
    expected = []
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_inner'
    test_cod = 3
    input_1 = ls_2
    input_2 = ls_4
    inputs = [input_1, input_2]
    expected = [1, 2, 3]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_inner'
    test_cod = 4
    input_1 = ls_4
    input_2 = ls_2
    inputs = [input_1, input_2]
    expected = [1, 2, 3]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_inner'
    test_cod = 5
    input_1 = ls_4_2
    input_2 = ls_2
    inputs = [input_1, input_2]
    expected = [1, 3, 2]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_left_anti'
    test_cod = 1
    input_1 = ls_0
    input_2 = ls_1
    inputs = [input_1, input_2]
    expected = []
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_left_anti'
    test_cod = 2
    input_1 = ls_1
    input_2 = ls_0
    inputs = [input_1, input_2]
    expected = ls_1
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_left_anti'
    test_cod = 3
    input_1 = ls_2
    input_2 = ls_4
    inputs = [input_1, input_2]
    expected = [4]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_left_anti'
    test_cod = 4
    input_1 = ls_4
    input_2 = ls_2
    inputs = [input_1, input_2]
    expected = [7]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_left_anti'
    test_cod = 5
    input_1 = ls_4_2
    input_2 = ls_2
    inputs = [input_1, input_2]
    expected = [7]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_anti'
    test_cod = 1
    input_1 = ls_0
    input_2 = ls_1
    inputs = [input_1, input_2]
    expected = ls_1
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_anti'
    test_cod = 2
    input_1 = ls_1
    input_2 = ls_0
    inputs = [input_1, input_2]
    expected = ls_1
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_anti'
    test_cod = 3
    input_1 = ls_2
    input_2 = ls_4
    inputs = [input_1, input_2]
    expected = [4, 7]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_anti'
    test_cod = 4
    input_1 = ls_4
    input_2 = ls_2
    inputs = [input_1, input_2]
    expected = [7, 4]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'join_anti'
    test_cod = 5
    input_1 = ls_4_2
    input_2 = ls_2
    inputs = [input_1, input_2]
    expected = [7, 4]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'transpose'
    test_cod = 1
    input_1 = lss_1
    inputs = [input_1]
    expected = lss_1_t
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'disjoint'
    test_cod = 1
    input_1 = ls_2
    input_2 = ls_3
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'disjoint'
    test_cod = 2
    input_1 = ls_3
    input_2 = ls_4
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'disjoint'
    test_cod = 3
    input_1 = ls_2
    input_2 = ls_4
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'disjoint'
    test_cod = 4
    input_1 = ls_3
    input_2 = ls_5
    inputs = [input_1, input_2]
    expected = True
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'disjoint'
    test_cod = 5
    input_1 = ls_4
    input_2 = ls_5
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'disjoint'
    test_cod = 6
    input_1 = ls_4_2
    input_2 = ls_5
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'disjoint'
    test_cod = 6
    input_1 = ls_4_2
    input_2 = ls_5
    inputs = [input_1, input_2]
    expected = False
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'lt_sort'
    test_cod = 1
    input_1 = ls_0
    input_2 = ls_0
    inputs = [input_1, input_2]
    expected = '='
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'lt_sort'
    test_cod = 2
    input_1 = ls_0
    input_2 = ls_1
    inputs = [input_1, input_2]
    expected = '<'
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'lt_sort'
    test_cod = 3
    input_1 = ls_1
    input_2 = ls_0
    inputs = [input_1, input_2]
    expected = '>'
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'lt_sort'
    test_cod = 4
    input_1 = ls_2
    input_2 = ls_4
    inputs = [input_1, input_2]
    expected = '<'
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'lt_sort'
    test_cod = 5
    input_1 = ls_2[:3]
    input_2 = ls_2
    inputs = [input_1, input_2]
    expected = '<'
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'generateMatrix'
    test_cod = 1
    input_1 = 2
    input_2 = 3
    inputs = [input_1, input_2]
    expected = [[[],[],[]],[[],[],[]]]
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'generateMatrix'
    test_cod = 2
    input_1 = 2
    input_2 = 1
    inputs = [input_1, input_2]
    expected = [[[]],[[]]]
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'generateMatrix'
    test_cod = 3
    input_1 = 1
    input_2 = 4
    inputs = [input_1, input_2]
    expected = [[[],[],[],[]]]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'generateMatrix'
    test_cod = 4
    input_1 = 4
    input_2 = 0
    inputs = [input_1, input_2]
    expected = [[],[],[],[]]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_position_bigger'
    test_cod = 1
    input_1 = ls_1
    input_2 = 2
    inputs = [input_1, input_2]
    expected = [3, 4]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_position_bigger'
    test_cod = 2
    input_1 = ls_1
    input_2 = 10
    inputs = [input_1, input_2]
    expected = [3, 4, 1, 0, 2]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'get_position_bigger'
    test_cod = 3
    input_1 = ls_1
    input_2 = 0
    inputs = [input_1, input_2]
    expected = []
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'get_position_bigger'
    test_cod = 4
    input_1 = ls_0
    input_2 = 3
    inputs = [input_1, input_2]
    expected = []
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'map'
    test_cod = 1
    input_1 = ls_0
    input_2 = str
    inputs = [input_1, input_2]
    expected = []
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'map'
    test_cod = 2
    input_1 = ls_1
    input_2 = str
    inputs = [input_1, input_2]
    expected = ['4', '5', '2', '9', '6']
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'fold'
    test_cod = 1
    input_1 = ls_0
    input_2 = max
    input_3 = 0
    inputs = [input_1, input_2, input_3]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'fold'
    test_cod = 2
    input_1 = ls_1
    input_2 = max
    input_3 = 0
    inputs = [input_1, input_2, input_3]
    expected = 9
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'fold'
    test_cod = 3
    input_1 = ls_1
    input_2 = min
    input_3 = 0
    inputs = [input_1, input_2, input_3]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'foldl'
    test_cod = 1
    input_1 = ls_0
    input_2 = max
    input_3 = 0
    inputs = [input_1, input_2, input_3]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'foldl'
    test_cod = 2
    input_1 = ls_1
    input_2 = max
    input_3 = 0
    inputs = [input_1, input_2, input_3]
    expected = 9
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'foldl'
    test_cod = 3
    input_1 = ls_1
    input_2 = min
    input_3 = 0
    inputs = [input_1, input_2, input_3]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'foldr'
    test_cod = 1
    input_1 = ls_0
    input_2 = max
    input_3 = 0
    inputs = [input_1, input_2, input_3]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'foldr'
    test_cod = 2
    input_1 = ls_1
    input_2 = max
    input_3 = 0
    inputs = [input_1, input_2, input_3]
    expected = 9
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'foldr'
    test_cod = 3
    input_1 = ls_1
    input_2 = min
    input_3 = 0
    inputs = [input_1, input_2, input_3]
    expected = 0
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'list_to_str'
    test_cod = 1
    input_1 = ls_0
    inputs = [input_1]
    expected = ''
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'list_to_str'
    test_cod = 1
    input_1 = ls_1
    inputs = [input_1]
    expected = '45296'
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'list_to_str'
    test_cod = 2
    input_1 = ls_1
    input_2 = '| '
    inputs = [input_1, input_2]
    expected = '4| 5| 2| 9| 6'
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'clone'
    test_cod = 1
    input_1 = ls_0
    inputs = [input_1]
    expected = ls_0
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'clone'
    test_cod = 2
    input_1 = ls_1
    inputs = [input_1]
    expected = ls_1
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'crossover'    
    test_cod = 1
    function = len
    input_1 = lss_1
    inputs = [input_1]
    expected = 4
    test.run_test(method_name, test_cod, inputs, expected, function)

    method_name = 'append_not_repeated'
    test_cod = 1
    input_1 = []
    input_2 = [0, 1, 2, 0]
    inputs = [input_1, input_2]
    expected = [0, 1, 2]
    test.run_test(method_name, test_cod, inputs, expected)

    method_name = 'append_not_repeated'
    test_cod = 1
    input_1 = [2, 4, 1, 0, 1, 3]
    input_2 = [0, 1, 2, 0, 5, 6]
    inputs = [input_1, input_2]
    expected = [2, 4, 1, 0, 1, 3, 5, 6]
    test.run_test(method_name, test_cod, inputs, expected)
    
    method_name = 'generate_random_subset'
    test_cod = 1
    function = len
    input_1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    input_2 = 9
    inputs = [input_1, input_2]
    expected = 9
    test.run_test(method_name, test_cod, inputs, expected, len)

    method_name = 'generate_random_subset'
    test_cod = 2
    function = len
    input_1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    input_2 = 5
    inputs = [input_1, input_2]
    expected = 5
    test.run_test(method_name, test_cod, inputs, expected, len)
    

    #----------------------------------------------------------------#
    #----------------------------------------------------------------#

    print(test.get_info())
